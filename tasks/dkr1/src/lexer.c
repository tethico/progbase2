#include <lexer.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

Token *Token_new(TokenType type, const char *name)
{
    Token *smToken = malloc(sizeof(Token));
    smToken->type = type;
    smToken->name = name;
    return smToken;
}
void Token_free(Token *self)
{
    free(self);
}



static const char * TokenType_toString_Arr[] = {
",",//TokenType_COMMA
"+",//     TokenType_PLUS,
"-",//    TokenType_MINUS,
"*",//    TokenType_MULT,
"/",//    TokenType_DIV,
"^",//    TokenType_POW,
"=",//    TokenType_ASSIGN,
"%",//    TokenType_MOD,
"WHILE",//    TokenType_WHILE,
"IF",//    TokenType_IF,
"ELSE",//    TokenType_ELSE,
"(",//    TokenType_BRACKET_L,
")",//    TokenType_BRACKET_R,
"{",//    TokenType_BLOCK_START,
"}",//    TokenType_BLOCK_END,
"!",//    TokenType_LOG_NOT,
"==",//    TokenType_LOG_EQ,
"!=",//    TokenType_LOG_NEQ,
"&&",//    TokenType_LOG_AND,
"||",//    TokenType_LOG_OR,
"<",//    TokenType_LOG_LESS,
">",//    TokenType_LOG_MORE,
"<=",//    TokenType_LOG_LESSEQ,
">=",//    TokenType_LOG_MOREEQ,
"BOOL",//    TokenType_BOOL,
"STR",//    TokenType_STRING,
"NUM",//    TokenType_NUMBER,
"VAR",//    TokenType_VAR,
"ARR",//    TokenType_ARRAY,
"NAME",//    TokenType_NAME,
";"//    TokenType_TERMINATOR
};

const char * TokenType_toString(TokenType tokenType)
{
    return TokenType_toString_Arr[tokenType];
}
int Lexer_splitTokens(const char *input, List *tokens)
{
    int i = 0;
    int status = 0;
    while (input[i] != '\0')
    {
        if (input[i] == ',')
        {
            Token *add = Token_new(TokenType_COMMA, ",");
            List_add(tokens, add);
        }
        if (input[i] == '+')
        {
            Token *add = Token_new(TokenType_PLUS, "+");
            List_add(tokens, add);
        }
        else if (input[i] == '-')
        {
            Token *add = Token_new(TokenType_MINUS, "-");
            List_add(tokens, add);
        }
        else if (input[i] == '%')
        {
            Token *add = Token_new(TokenType_MOD, "%");
            List_add(tokens, add);
        }
        else if (input[i] == '!' && input[i+1] == '=')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_NEQ, "!=");
            List_add(tokens, add);
        }
        else if (input[i] == '!')
        {
            Token *add = Token_new(TokenType_LOG_NOT, "!");
            List_add(tokens, add);
        }
        else if (input[i] == '=' && input[i+1] == '=')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_EQ, "==");
            List_add(tokens, add);
        }
        else if (input[i] == '&' && input[i+1] == '&')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_AND, "&&");
            List_add(tokens, add);
        }
        else if (input[i] == '|' && input[i+1] == '|')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_OR, "||");
            List_add(tokens, add);
        }
        else if (input[i] == '<' && input[i+1] == '=')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_LESSEQ, "<=");
            List_add(tokens, add);
        }
        else if (input[i] == '>' && input[i+1] == '=')
        {
            i++;
            Token *add = Token_new(TokenType_LOG_MOREEQ, ">=");
            List_add(tokens, add);
        }
        else if (input[i] == '<')
        {
            Token *add = Token_new(TokenType_LOG_LESS, "<");
            List_add(tokens, add);
        }
        else if (input[i] == '>')
        {
            Token *add = Token_new(TokenType_LOG_MORE, ">");
            List_add(tokens, add);
        }
        else if (input[i] == '=')
        {
            Token *add = Token_new(TokenType_ASSIGN, "=");
            List_add(tokens, add);
        }
        else if (input[i] == ';')
        {
            Token *add = Token_new(TokenType_TERMINATOR, "term");
            List_add(tokens, add);
        }
        else if (input[i] == '*')
        {
            Token *add = Token_new(TokenType_MULT, "mult");
            List_add(tokens, add);
        }
        else if (input[i] == '(')
        {
            Token *add = Token_new(TokenType_BRACKET_L, "");
            List_add(tokens, add);
        }
        else if (input[i] == ')')
        {
            Token *add = Token_new(TokenType_BRACKET_R, "");
            List_add(tokens, add);
        }
        else if (input[i] == '{')
        {
            Token *add = Token_new(TokenType_BLOCK_START, "");
            List_add(tokens, add);
        }
        else if (input[i] == '}')
        {
            Token *add = Token_new(TokenType_BLOCK_END, "");
            List_add(tokens, add);
        }
        
        else if (input[i] == 'i' && input[i + 1] == 'f' && !isalnum(input[i + 2]) )
        {
            i = i + 1;
            Token *add = Token_new(TokenType_IF, "if");
            List_add(tokens, add);
        }
        else if (input[i] == 'e' && input[i + 1] == 'l' && input[i + 2] == 's' && input[i + 3] == 'e' && !isalnum(input[i + 4]) )
        {
            i = i + 3;
            Token *add = Token_new(TokenType_ELSE, "else");
            List_add(tokens, add);
        }
        else if (input[i] == 't' && input[i + 1] == 'r' && input[i + 2] == 'u' && input[i + 3] == 'e' && !isalnum(input[i + 4]) )
        {
            i = i + 3;
            Token *add = Token_new(TokenType_BOOL, "true");
            List_add(tokens, add);
        }
        //while
        else if (input[i] == 'w' && input[i + 1] == 'h' && input[i + 2] == 'i' && input[i + 3] == 'l' && input[i + 4] == 'e' && !isalnum(input[i + 5]))
        {
            i = i + 4;
            Token *add = Token_new(TokenType_WHILE, "while");
            List_add(tokens, add);
        }
        else if (input[i] == 'f' && input[i + 1] == 'a' && input[i + 2] == 'l' && input[i + 3] == 's' && input[i + 4] == 'e' && !isalnum(input[i + 5]))
        {
            i = i + 4;
            Token *add = Token_new(TokenType_BOOL, "false");
            List_add(tokens, add);
        }
        else if (input[i] == 'v' && input[i + 1] == 'a' && input[i + 2] == 'r' && !isalnum(input[i + 3]))
        {
            i = i + 2;
            Token *add = Token_new(TokenType_VAR, "var");
            List_add(tokens, add);
        }
        else if (input[i] == '/')
        {
            Token *add = Token_new(TokenType_DIV, "/");
            List_add(tokens, add);
        }
        else if (input[i] == '^')
        {
            Token *add = Token_new(TokenType_POW, "pow");
            List_add(tokens, add);
        }
        else if (input[i] == '"')
        {
            i++;
            StringBuffer *sb = StringBuffer_new();
            while (input[i] != '"')
            {
                StringBuffer_appendChar(sb, input[i]);
                i++;
            }
            Token *add = Token_new(TokenType_STRING, StringBuffer_toNewString(sb));
            List_add(tokens, add);
        }
        else if (isdigit(input[i]))
        {
            int j = i;
            int k = 0;
            bool dotRead = false;
            StringBuffer *sb = StringBuffer_new();
            while (isdigit(input[j]) || (input[j] =='.' && !dotRead))
            {
                if(input[j] =='.')
                {
                    dotRead = true;
                }
                StringBuffer_appendChar(sb, input[j]);
                j++;
                k++;
            }
            Token *add = Token_new(TokenType_NUMBER, StringBuffer_toNewString(sb));
            List_add(tokens, add);
            i = i + k - 1;
        }
        else if (isalpha(input[i]))
        {
            int j = i;
            int k = 0;
            StringBuffer *sb = StringBuffer_new();
            while (isalpha(input[j]))
            {
                StringBuffer_appendChar(sb, input[j]);
                j++;
                k++;
            }
            Token *add = Token_new(TokenType_NAME, StringBuffer_toNewString(sb));
            List_add(tokens, add);
            i = i + k - 1;
        }else if(isspace(input[i])){
        
        }
        i++;
    }
    return status;
}
void Lexer_clearTokens(List *tokens)
{
    for (int i = List_count(tokens); i > 0; i--)
    {
        List_removeAt(tokens, i);
    }
}

void Lexer_printTokens(List *tokens)
{
    Iterator * begin = List_getNewBeginIterator(tokens);
    Iterator * end = List_getNewEndIterator(tokens);

    while(!Iterator_equals(begin,end)){
        Token *buff = Iterator_value(begin);
        printf("<");
        printf(" %s ",TokenType_toString(buff->type));
        if (buff->type == TokenType_NUMBER)
        {
            printf(", %s ", buff->name);
        }
        if (buff->type == TokenType_STRING)
        {
            printf(", %s ", buff->name);
        }
        if (buff->type == TokenType_NAME)
        {
            printf(", %s ", buff->name);
        }
        if (buff->type == TokenType_BOOL)
        {
            printf(", %s ", buff->name);
        }
        printf(">");
        Iterator_next(begin);
    }
    Iterator_free(begin);
    Iterator_free(end);
}

bool Token_equals(Token *self, Token *other)
{
    if (self->type == other->type && strcmp(self->name, other->name) == 0)
    {
        return true;
    }
    return false;
}
