#include "station_string.h"

void StationStruct_toString(char *finString, Station *station)
{
    sprintf(finString, "%s %i %lf %lf %lf", station->name, station->numberOfExits, station->price, station->Timetable.timeBetween, station->Timetable.openTime);
}

Station *String_toStationStruct(char *srcString)
{
    Station *station = malloc(sizeof(Station)); 
    sscanf(srcString, "%s %i %lf %lf %lf", station->name, &station->numberOfExits, &station->price, &station->Timetable.timeBetween, &station->Timetable.openTime);
    return station;
}