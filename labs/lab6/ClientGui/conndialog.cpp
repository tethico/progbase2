#include "conndialog.h"
#include "ui_conndialog.h"
#include <QTcpSocket>

connDialog::connDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::connDialog)
{
    ui->setupUi(this);
}

connDialog::~connDialog()
{
    delete ui;
}



void connDialog::on_buttonBox_accepted()
{
    this->address = ui->lineEdit->text();
    this->port = ui->spinBox->value();
    QTcpSocket * socket = new QTcpSocket();
    socket->connectToHost(this->address,this->port);
    if(socket->state() != QTcpSocket::NotOpen){
        bSuccessfulConn = true;
        socket->disconnectFromHost();
    }
}


QHostAddress connDialog::getAddress()
{
    return this->address;
}

int connDialog::getPort()
{
    return this->port;
}
