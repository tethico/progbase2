#include "filechoicedialog.h"
#include "ui_filechoicedialog.h"

fileChoiceDialog::fileChoiceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fileChoiceDialog)
{
    ui->setupUi(this);
}

fileChoiceDialog::~fileChoiceDialog()
{
    delete ui;
}

QString fileChoiceDialog::getFileName()
{
    return fileName;
}

void fileChoiceDialog::on_buttonBox_accepted()
{
    this->bAccepted = true;
    fileName = ui->lineEdit->text();
}
