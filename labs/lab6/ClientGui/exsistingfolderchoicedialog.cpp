#include "exsistingfolderchoicedialog.h"
#include "ui_exsistingfolderchoicedialog.h"
#include <QDebug>
exsistingFolderChoiceDialog::exsistingFolderChoiceDialog(QStringList list,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exsistingFolderChoiceDialog)
{
    ui->setupUi(this);
    qDebug() << "initializing folder contents";
    for(int i = 2; i < list.size(); i++)
    {
        ui->listWidget->addItem(list.at(i));
    }
}

exsistingFolderChoiceDialog::~exsistingFolderChoiceDialog()
{
    delete ui;
}

QString exsistingFolderChoiceDialog::getName()
{
    return this->fileName;
}

void exsistingFolderChoiceDialog::on_buttonBox_accepted()
{
    if(ui->listWidget->selectedItems().count() != 0)
    {
        fileName = ui->listWidget->selectedItems().at(0)->text();
    }
}
