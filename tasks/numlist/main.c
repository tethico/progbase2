#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <numlist_ext.h>
#include <numlist.h>
#include <time.h>

static int MAX_LENGTH = 10;

int main (){
    NumList * list = Numlist_new();
    int arr[MAX_LENGTH];
    srand(time(NULL));
    for(int i = 0; i < MAX_LENGTH;i++){
        arr[i] = rand() % 200 - 100;
    }
    for(int i = 0; i < MAX_LENGTH;i++){
        Numlist_add(list, arr[i]);
    }
    Numlist_print(list);
    printf("Now deleting negative numbers");
    Numlist_deleteNegative(list);
    Numlist_print(list);
    Numlist_free(list);
}