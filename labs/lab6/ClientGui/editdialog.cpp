#include "editdialog.h"
#include "ui_editdialog.h"

editDialog::editDialog(QString name, int numOfExits, double price, QTime interval, QTime timeOpen,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editDialog)
{
    ui->setupUi(this);
        ui->lineEdit->setText(name);
        ui->spinBox->setValue(numOfExits);
        ui->doubleSpinBox->setValue(price);
        ui->timeEdit->setTime(interval);
        ui->timeEdit_2->setTime(timeOpen);
}

editDialog::~editDialog()
{
    delete ui;
}

MetroStation editDialog::getStation()
{
    return this->station;
}

void editDialog::on_buttonBox_accepted()
{
    bAccepted = true;
    station.setName(ui->lineEdit->text());
    station.setNumOfExits(ui->spinBox->value());
    station.setPrice(ui->doubleSpinBox->value());
    station.setTimetable(ui->timeEdit_2->time(),ui->timeEdit->time());
}
