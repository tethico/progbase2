#ifndef CONNDIALOG_H
#define CONNDIALOG_H

#include <QDialog>
#include <QHostAddress>

namespace Ui {
class connDialog;
}

class connDialog : public QDialog
{
    Q_OBJECT

public:
    explicit connDialog(QWidget *parent = 0);
    ~connDialog();
    QHostAddress getAddress();
    int getPort();
    bool bSuccessfulConn = false;
private slots:

    void on_buttonBox_accepted();

private:
    QHostAddress address;
    int port;
    Ui::connDialog *ui;
};

#endif // CONNDIALOG_H
