#pragma once

#include <string>
#include <vector>

class ScrumMaster {
public:
    std::string name;
    int age;
    double efficencyMark;
    std::vector<std::string> team;
public:
    ScrumMaster(std::string name, int age, double efficencyMark, std::vector<std::string> team);
    ScrumMaster();
};

