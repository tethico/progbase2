#pragma once

typedef enum {
   AstNodeType_COMMA,
   AstNodeType_PLUS,
   AstNodeType_MINUS,
   AstNodeType_MULT,
   AstNodeType_DIV,
   AstNodeType_POW,
   AstNodeType_ASSIGN,
   AstNodeType_MOD,
   //
   AstNodeType_PROGRAM,
   AstNodeType_VARDECL,
   AstNodeType_ARGLIST,
   AstNodeType_WHILE,
   AstNodeType_IF,
   AstNodeType_BLOCK,
   //
   AstNodeType_LOG_NOT,
   AstNodeType_LOG_EQ,
   AstNodeType_LOG_NEQ,
   AstNodeType_LOG_AND,
   AstNodeType_LOG_OR,
   AstNodeType_LOG_LESS,
   AstNodeType_LOG_MORE,
   AstNodeType_LOG_LESSEQ,
   AstNodeType_LOG_MOREEQ,
   //
   AstNodeType_STRING,
   AstNodeType_NUMBER,
   AstNodeType_NAME,

   AstNodeType_UNK
} AstNodeType;

typedef struct __AstNode AstNode;
struct __AstNode {
   AstNodeType type;
   const char * name;
   // @todo extra data
};

AstNode * AstNode_new(AstNodeType type, const char * name);
void AstNode_free(AstNode * self);
