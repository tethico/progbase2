#-------------------------------------------------
#
# Project created by QtCreator 2018-06-02T03:25:54
#
#-------------------------------------------------

QT       += core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets gui xml network

TARGET = ClientGui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    adddialog.cpp \
    editdialog.cpp \
    filechoicedialog.cpp \
    conndialog.cpp \
    exsistingfolderchoicedialog.cpp

HEADERS += \
    adddialog.h \
    mainwindow.h \
    editdialog.h \
    filechoicedialog.h \
    conndialog.h \
    exsistingfolderchoicedialog.h

FORMS += \
        mainwindow.ui \
    adddialog.ui \
    editdialog.ui \
    filechoicedialog.ui \
    conndialog.ui \
    exsistingfolderchoicedialog.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/release/ -lStationParser
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/debug/ -lStationParser
else:unix: LIBS += -L$$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/ -lStationParser

INCLUDEPATH += $$PWD/../../../../../StationParser
DEPENDPATH += $$PWD/../../../../../StationParser

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/release/libStationParser.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/debug/libStationParser.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/release/StationParser.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/debug/StationParser.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../build-StationParser-Desktop_Qt_5_10_0_clang_64bit-Debug/libStationParser.a
