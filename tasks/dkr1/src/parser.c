#include <parser.h>
#include <iterator.h>
#include <iterator.h>
#include <lexer.h>
#include <tree.h>
#include <ast.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

typedef struct
{
    Iterator *tokens;
    Iterator *tokens_end;
    char *error;
    int level;
} Parser;

static Tree *accept(Parser *self, TokenType tokenType);
static Tree *expect(Parser *self, TokenType tokenType);

typedef Tree *(*GrammarRule)(Parser *parser);

static bool ebnf_mult(Parser *parser, List *nodes, GrammarRule rule);
static Tree *ebnf_select(Parser *parser, GrammarRule rules[], size_t rulesLen);
static Tree *ebnf_selectLexemes(Parser *parser, TokenType types[], size_t typesLen);
static Tree *ebnf_ap_main_rule(Parser *parser, GrammarRule next, GrammarRule pr);
static Tree *ebnf_ap_recursive_rule(Parser *parser, TokenType types[], size_t lypesLen, GrammarRule next, GrammarRule pr);
static Tree *NAME(Parser *parser);
static Tree *NAME_exp(Parser *parser);
static Tree *STRING(Parser *parser);
static Tree *STRING_exp(Parser *parser);
static Tree *NUMBER(Parser *parser);
static Tree *NUMBER_exp(Parser *parser);
static Tree *prog(Parser *parser);
static Tree *var_decl(Parser *parser);
static Tree *st(Parser *parser);
static Tree *expr(Parser *parser);
static Tree *expr_st(Parser *parser);
static Tree *select_st(Parser *parser);
static Tree *block_st(Parser *parser);
static Tree *iter_st(Parser *parser);
static Tree *assign(Parser *parser);
static Tree *assign_pr(Parser *parser);
static Tree *logic_or(Parser *parser);
static Tree *logic_or_pr(Parser *parser);
static Tree *logic_and(Parser *parser);
static Tree *logic_and_pr(Parser *parser);
static Tree *comp(Parser *parser);
static Tree *comp_pr(Parser *parser);
static Tree *relat(Parser *parser);
static Tree *relat_pr(Parser *parser);
static Tree *add(Parser *parser);
static Tree *add_pr(Parser *parser);
static Tree *mult(Parser *parser);
static Tree *mult_pr(Parser *parser);
static Tree *unary(Parser *parser);
static Tree *prim(Parser *parser);
static Tree *var_or_call(Parser *parser);
static Tree *parentheses(Parser *parser);
static Tree *func_call(Parser *parser);
static Tree *arg_list(Parser *parser);
static bool eoi(Parser *self);

Tree *Parser_buildNewAstTree(List *tokens)
{
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokens_end = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1};
    Tree *progNode = prog(&parser);
    if (parser.error)
    {
        fprintf(stderr, "ERROR: %s", parser.error);
        free((void *)parser.error);
        return false;
    }
    else
    {
        if (!Iterator_equals(parser.tokens, parser.tokens_end))
        {
            Token *token = Iterator_value(parser.tokens);
            fprintf(stderr, "ERROR: unexpected token %s", (char *)TokenType_toString(token->type));
            return false;
        }
    }
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokens_end);
    return progNode;
}
// bool Parser_match(List *tokens)
// {
//     Parser parser = {
//         .tokens = List_getNewBeginIterator(tokens),
//         .tokens_end = List_getNewEndIterator(tokens),
//         .error = NULL,
//         .level = -1
//     };
//     bool match = prog(&parser);
//     if (parser.error)
//     {
//         fprintf(stderr, "ERROR: %s" ,parser.error);
//         free((void *)parser.error);
//         return false;
//     }
//     else
//     {
//         if(!Iterator_equals(parser.tokens,parser.tokens_end))
//             {
//                 Token * token = Iterator_value(parser.tokens);
//                 fprintf(stderr, "ERROR: unexpected token %s" ,(char*)TokenType_toString(token->type));
//                 return false;
//             }
//     }
//     Iterator_free(parser.tokens);
//     Iterator_free(parser.tokens_end);
//     return match;
// }

static AstNodeType TokenType_toAstNodeType(TokenType type)
{
    switch (type)
    {
    case TokenType_ASSIGN:
        return AstNodeType_ASSIGN;
    case TokenType_PLUS:
        return AstNodeType_PLUS;
    case TokenType_MINUS:
        return AstNodeType_MINUS;
    case TokenType_MULT:
        return AstNodeType_MULT;
    case TokenType_DIV:
        return AstNodeType_DIV;
    case TokenType_MOD:
        return AstNodeType_MOD;
    case TokenType_LOG_EQ:
        return AstNodeType_LOG_EQ;
    case TokenType_LOG_NEQ:
        return AstNodeType_LOG_NEQ;
    case TokenType_LOG_NOT:
        return AstNodeType_LOG_NOT;
    case TokenType_LOG_LESS:
        return AstNodeType_LOG_LESS;
    case TokenType_LOG_MORE:
        return AstNodeType_LOG_MORE;
    case TokenType_LOG_LESSEQ:
        return AstNodeType_LOG_LESSEQ;
    case TokenType_LOG_MOREEQ:
        return AstNodeType_LOG_MOREEQ;
    case TokenType_LOG_AND:
        return AstNodeType_LOG_AND;
    case TokenType_LOG_OR:
        return AstNodeType_LOG_OR;
    case TokenType_NAME:
        return AstNodeType_NAME;
    case TokenType_NUMBER:
        return AstNodeType_NUMBER;
    case TokenType_STRING:
        return AstNodeType_STRING;
    default:
        return AstNodeType_UNK;
    }
}
static bool eoi(Parser *self)
{
    return Iterator_equals(self->tokens, self->tokens_end);
}

static Tree *accept(Parser *self, TokenType tokenType)
{
    if (eoi(self))
        return false;
    Token *token = Iterator_value(self->tokens);
    if (token->type == tokenType)
    {
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char *astName = strdup(token->name);
        AstNode *node = AstNode_new(astType, astName);
        Tree *tree = Tree_new(node);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}

static Tree *expect(Parser *parser, TokenType tokenType)
{
    Tree *tree = accept(parser, tokenType);
    if (tree != NULL)
    {
        return tree;
    }
    const char *currentTokenType = eoi(parser)
                                       ? "end of input"
                                       : TokenType_toString(((Token *)Iterator_value(parser->tokens))->type);
    StringBuffer *sb = StringBuffer_new();
    StringBuffer_appendFormat(sb, "expected '%s' got '%s' .\n", TokenType_toString(tokenType), currentTokenType);
    parser->error = StringBuffer_toNewString(sb);
    StringBuffer_free(sb);
    return NULL;
}

static bool ebnf_mult(Parser *parser, List *nodes, GrammarRule rule)
{
    Tree *node = NULL;
    while ((node = rule(parser)) && !parser->error)
    {
        List_add(nodes, node);
    }
    return parser->error == NULL ? true : false;
}

static Tree *ebnf_select(Parser *parser, GrammarRule rules[], size_t rulesLen)
{
    Tree *node = NULL;
    for (int i = 0; i < rulesLen && !node; i++)
    {
        GrammarRule rule = rules[i];
        node = rule(parser);
        if (parser->error)
        {
            return NULL;
        }
    }
    return node;
}

static Tree *ebnf_selectLexemes(Parser *parser, TokenType types[], size_t typesLen)
{
    Tree *node = NULL;
    for (int i = 0; i < typesLen && !node; i++)
    {
        node = accept(parser, types[i]);
    }
    return node;
}

static Tree *ebnf_ap_main_rule(Parser *parser, GrammarRule next, GrammarRule pr)
{
    Tree *nextNode = next(parser);
    if (nextNode)
    {
        Tree *prNode = pr(parser);
        if (prNode)
        {
            List_insert(prNode->children, nextNode, 0);
            return prNode;
        }
        return nextNode;
    }
    return NULL;
}

static Tree * ebnf_ap_recursive_rule(Parser *parser, TokenType types[], size_t typesLen, GrammarRule next, GrammarRule pr)
{
    Tree * opNode = ebnf_selectLexemes(parser,types,typesLen);
    if(opNode == NULL) return NULL;
    Tree * node = NULL;
    Tree * nextNode = next(parser);
    Tree * apNode = pr(parser);
    if(apNode){
        List_insert(apNode->children,nextNode,0);
        node = apNode;
    } else {
        node = nextNode;
    }
    List_add(opNode->children,node);
    return opNode;
}

static Tree *NAME(Parser *parser)
{

    return accept(parser, TokenType_NAME);
}

static Tree *NAME_exp(Parser *parser)
{
    return expect(parser, TokenType_NAME);
}
static Tree *STRING(Parser *parser)
{

    return accept(parser, TokenType_STRING);
}

static Tree *NUMBER(Parser *parser)
{

    return accept(parser, TokenType_NUMBER);
}

static Tree *prog(Parser *parser)
{

    Tree *progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, "program"));
    (void)ebnf_mult(parser, progNode->children, var_decl);
    (void)ebnf_mult(parser, progNode->children, st);
    return progNode;
}
static Tree *var_decl(Parser *parser)
{

    if (!accept(parser, TokenType_VAR))
        return NULL;
    Tree *nameNode = NAME_exp(parser);
    if (!nameNode)
        return NULL;
    if (!expect(parser, TokenType_ASSIGN))
    {
        return NULL;
    }
    Tree *exprNode = expr(parser);
    if (exprNode == NULL)
    {
        return NULL;
    }
    if (!expect(parser, TokenType_TERMINATOR))
    {
        return NULL;
    }
    Tree *varDecl = Tree_new(AstNode_new(AstNodeType_VARDECL, "variable declaration"));
    List_add(varDecl->children, nameNode);
    List_add(varDecl->children, exprNode);
    return varDecl;
}
static Tree *st(Parser *parser)
{

    return ebnf_select(parser, (GrammarRule[]){block_st, select_st, iter_st, expr_st}, 4);
}
static Tree *expr(Parser *parser)
{

    return assign(parser);
}
static Tree *expr_st(Parser *parser)
{

    Tree *exprNode = expr(parser);
    if (exprNode)
    {
        expect(parser, TokenType_TERMINATOR);
    }
    else
    {
        accept(parser, TokenType_TERMINATOR);
    }
    return exprNode;
}
static Tree *select_st(Parser *parser)
{


    if (!accept(parser, TokenType_IF) || !expect(parser, TokenType_BRACKET_L))
        return NULL;
    Tree *testExprNode = expr(parser);
    if (testExprNode == NULL)
    {
        //@todo error
        return NULL;
    }
    if (!expect(parser, TokenType_BRACKET_R))
    {
        //@todo clear treeExpr
        //@todo error
        return NULL;
    }
    Tree *stNode = st(parser);
    if (stNode == NULL)
    {
        //@todo clear treeExpr
        //@todo error
        return NULL;
    }
    Tree *ifNode = Tree_new(AstNode_new(AstNodeType_IF, "if"));
    List_add(ifNode->children, testExprNode);
    List_add(ifNode->children, stNode);
    if (accept(parser, TokenType_ELSE))
    {
        Tree *elseNode = st(parser);
        if (elseNode == NULL || parser->error)
        {
            //@todo error

            return NULL;
        }
        List_add(ifNode->children, elseNode);
    }
    return ifNode;
}
static Tree *block_st(Parser *parser)
{

    if (!accept(parser, TokenType_BLOCK_START))
        return NULL;
    Tree *blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, "block"));
    if (ebnf_mult(parser, blockNode->children, st))
    {
        (void)expect(parser, TokenType_BLOCK_END);
    }
    return blockNode;
}
static Tree *iter_st(Parser *parser)
{

    if (!accept(parser, TokenType_WHILE) || !expect(parser, TokenType_BRACKET_L))
        return NULL;
    Tree *testExprNode = expr(parser);
    if (testExprNode == NULL)
    {
        //@todo error
        return NULL;
    }
    if (!expect(parser, TokenType_BRACKET_R))
    {
        //@todo clear treeExpr
        //@todo error
        return NULL;
    }
    Tree *stNode = st(parser);
    if (stNode == NULL)
    {
        //@todo clear treeExpr
        //@todo error
        return NULL;
    }
    Tree *whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, "while"));
    List_add(whileNode->children, testExprNode);
    List_add(whileNode->children, stNode);
    return whileNode;
}
static Tree *assign(Parser *parser)
{

    return ebnf_ap_main_rule(parser, logic_or, assign_pr);
}
static Tree *assign_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_ASSIGN}, 1, logic_or, assign_pr);
}
static Tree *logic_or(Parser *parser)
{

    return ebnf_ap_main_rule(parser, logic_and, logic_or_pr);
}
static Tree *logic_or_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_LOG_OR}, 1, logic_and, logic_or_pr);
}
static Tree *logic_and(Parser *parser)
{

    return ebnf_ap_main_rule(parser, comp, logic_and_pr);
}
static Tree *logic_and_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_LOG_AND}, 1, comp, logic_and_pr);
}
static Tree *comp(Parser *parser)
{

    return ebnf_ap_main_rule(parser, relat, comp_pr);
}
static Tree *comp_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_LOG_EQ, TokenType_LOG_NEQ}, 1, relat, comp_pr);
}
static Tree *relat(Parser *parser)
{

    return ebnf_ap_main_rule(parser, add, relat_pr);
}
static Tree *relat_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_LOG_LESS, TokenType_LOG_MORE, TokenType_LOG_MOREEQ, TokenType_LOG_LESSEQ}, 4, add, relat_pr);
}
static Tree *add(Parser *parser)
{

    return ebnf_ap_main_rule(parser, mult, add_pr);
}
static Tree *add_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_PLUS, TokenType_MINUS}, 2, mult, add_pr);
}
static Tree *mult(Parser *parser)
{

    return ebnf_ap_main_rule(parser, unary, mult_pr);
}
static Tree *mult_pr(Parser *parser)
{

    return ebnf_ap_recursive_rule(parser, (TokenType[]){TokenType_MULT, TokenType_DIV, TokenType_MOD}, 3, unary, mult_pr);
}
static Tree *unary(Parser *parser)
{

    Tree *opNode = ebnf_selectLexemes(parser, (TokenType[]){TokenType_PLUS, TokenType_MINUS, TokenType_LOG_NOT}, 3);
    Tree *primNode = prim(parser);
    //@todo check NULL prim
    if (opNode)
    {
        List_add(opNode->children, primNode);
        return opNode;
    }
    return primNode;
}

static Tree *prim(Parser *parser)
{

    return ebnf_select(parser, (GrammarRule[]){NUMBER, STRING, var_or_call, parentheses}, 4);
}

static Tree *var_or_call(Parser *parser)
{

    Tree *varNode = NAME(parser);
    Tree *argListNode = func_call(parser);
    if (argListNode)
    {
        List_add(varNode->children, argListNode);
    }
    return varNode;
}
static Tree *parentheses(Parser *parser)
{

    ;
    if (!accept(parser, TokenType_BRACKET_L))
    {
        return NULL;
    }
    Tree *exprNode = expr(parser);
    expect(parser, TokenType_BRACKET_R); //@todo
    return exprNode;
}
static Tree *func_call(Parser *parser)
{

    if (!accept(parser, TokenType_BRACKET_L))
    {
        return NULL;
    }
    Tree *argListNode = arg_list(parser);
    expect(parser, TokenType_BRACKET_R); //@todo
    return argListNode;
}
static Tree *arg_list(Parser *parser)
{
    Tree *exprNode = expr(parser);
    Tree *argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, "arglist"));
    if (exprNode)
    {
        List_add(argListNode->children, exprNode);
        while (true)
        {
            if (!accept(parser, TokenType_COMMA))
                break;
            exprNode = expr(parser);
            if (exprNode)
            {
                List_add(argListNode->children, exprNode);
            }
            else
            {
                break;
            }
        }
        return argListNode;
    }
    return NULL;
}