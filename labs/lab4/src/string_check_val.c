#include "string_check_val.h"

bool isInt(char *buf)
{
    if (buf[0] == '\n')
        return false;
    for (int i = 0; i < strlen(buf); i++)
        if (!(isdigit(buf[i]) || buf[i] == '\n'))
        {
            return false;
        }
        else
        {
            if (i == strlen(buf) - 1)
                return true;
        }
    return false; 
}

bool isDouble(char *buf)
{
    if (buf[0] == '\n')
        return false;
    for (int i = 0; i < strlen(buf); i++)
    {
        if (!(isdigit(buf[i]) || buf[i] == '.' || buf[i] == '\n'))
        {
            return false;
        }
        else
        {
            if (i == strlen(buf) - 1)
                return true;
        }
    }
    return false; 
}