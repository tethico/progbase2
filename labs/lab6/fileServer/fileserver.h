#pragma once
#include <QObject>
#include <QTcpServer>
#include <stationparser.h>
#include <QFile>
class fileServer : public QObject
{

public:
    explicit fileServer(QObject *parent = nullptr);
private:
    QTcpServer* server;
    QList<MetroStation> activeServerList;
    QString dir = "/Users/danylokobryn/progbase2/progbase2/labs/lab6/build-fileServer-Desktop_Qt_5_10_0_clang_64bit-Debug/data/";
    Q_OBJECT
    QFile file;
public slots:
    void onNewConnected();
    void onRequest();
    void onDisconnected();
};

