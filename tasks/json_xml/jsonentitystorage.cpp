#include "jsonentitystorage.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <iostream>
#include <fstream>
JsonEntityStorage::JsonEntityStorage(std::string name) : EntityStorage(name){
    this->_name = name;
}

std::vector<ScrumMaster> JsonEntityStorage::load()
{
    std::vector<ScrumMaster> toRet;
    std::string str;
    str = "../json_xml/" + this->_name;
    std::ifstream file;
    file.open(str);
    if (!file) {
            std::cout << "Unable to open file in method";
            return toRet;
        }
    std::string buff;
    std::string docString;
    while(file.good()){
        std::getline(file,buff,'\0');
        docString.append(buff);
    }
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(docString),
           &err);
    if (err.error != QJsonParseError::NoError) {
        std::string msg = err.errorString().toStdString();
            puts(msg.c_str());
    }
    QJsonObject arrayOfSM = doc.object();
    QJsonArray scrumMasters = arrayOfSM.take("Scrum Masters").toArray();
    for(int i = 0; i < scrumMasters.size();i++){
        ScrumMaster master;
        QJsonObject smaster = scrumMasters.at(i).toObject();
        master.age = smaster.take("age").toInt();
        master.name = smaster.take("name").toString().toStdString();
        master.efficencyMark = smaster.take("efficency mark").toDouble();
        QJsonArray team = smaster.take("team").toArray();
        for(int j = 0; j < team.size(); j++){
            master.team.push_back(team.at(j).toString().toStdString());
        }
        toRet.push_back(master);
    }
    return toRet;
}

void JsonEntityStorage::save(std::vector<ScrumMaster> &entities)
{
    QJsonDocument doc;
    QJsonObject array;
    QJsonArray scrumMasters;
    for(unsigned int i = 0; i < entities.size(); i++)
    {
        QJsonObject master;
        master.insert("name",entities.at(i).name.c_str());
        master.insert("age",entities.at(i).age);
        master.insert("efficency mark",entities.at(i).efficencyMark);
        //aray
        QJsonArray team;
        for(unsigned int j = 0; j < entities.at(i).team.size(); j++){
            team.append(entities.at(i).team.at(j).c_str());
        }
        master.insert("team",team);
        scrumMasters.append(master);
    }
    array.insert("Scrum Masters", scrumMasters);
    doc.setObject(array);
    std::string str;
    str = "../json_xml/" + this->_name;
    FILE * f = fopen(str.c_str(),"w+");
    fprintf(f,"%s",doc.toJson().toStdString().c_str());
    fclose(f);
}
