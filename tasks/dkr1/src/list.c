#include <stdlib.h>
#include <list.h>
#include <stdio.h>
#include <iterator.h>
#include <assert.h>

struct __List {
    size_t capaicty;
    size_t length;
    void** items;
};

static const int INITIAL_CAPASITY = 16;

List * List_new(void)
{
    List *self = malloc(sizeof(List));
    self->capaicty = INITIAL_CAPASITY;
    self->length = 0;
    self->items = malloc(sizeof(void*) * INITIAL_CAPASITY);
    return self; 
}

void List_add(List * self, void* value)
{
    if(self->length == self->capaicty){
        self->capaicty = self->capaicty*2;
        self->items = realloc(self->items,sizeof(void*)*self->capaicty);
    }
    self->items[self->length] = value;
    self->length++;
}

void List_free(List * self)
{
    free(self->items);
    free(self);
}

void* List_at(List * self, size_t index)
{
    if(index > self->length){
        printf("Error, index is bigger than lentgh");
        abort();
    }
    return self->items[index];
}

void List_removeAt(List * self, size_t index)
{
    if(index > self->length){
        printf("Error, index is bigger than lentgh");
        abort();
    }
    for(int i = index; i < self->length;i++){
        self->items[i] = self->items[i+1];
    }
    self->length--;
}


void List_insert(List * self, void* value,size_t index)
{
    if(index > self->length){
        printf("Error, index is bigger than lentgh");
        abort();
    }
    if(self->length == self->capaicty){
        self->items = realloc(self->items, sizeof(self) + sizeof(int));
    }
    self->length++;
    for(int i = index; i < self->length - 1;i++)
    {
        self->items[i + 1] = self->items[i];
    }
    self->items[index] = value;
}

size_t List_count(List * self)
{
    return self->length;
}


// iterator 
struct __Iterator
{
    List * list;
    int index;
};

Iterator * Iterator_new(List * list, int index)
{
    Iterator * self = malloc(sizeof(Iterator));
    self->list = list;
    self->index = index;
    return self;
}

Iterator * List_getNewBeginIterator (List * self)
{
    return Iterator_new(self, 0);
}
Iterator * List_getNewEndIterator   (List * self)
{
    return Iterator_new(self, List_count(self));
}

void * Iterator_value    (Iterator * self)
{
    assert(self->index < List_count(self->list));
    return List_at(self->list,self->index);
}
void   Iterator_next     (Iterator * self)
{
    self->index++;
}
void   Iterator_prev     (Iterator * self)
{
    self->index++;
}
void   Iterator_advance  (Iterator * self, IteratorDistance n)
{
    self->index += n;
}
bool   Iterator_equals   (Iterator * self, Iterator * other)
{
    return self->list == other->list
    && self->index == other->index;
}
IteratorDistance Iterator_distance (Iterator * begin, Iterator * end)
{
    return end->index - begin->index;
}

void Iterator_free  (Iterator * self)
{
    free(self);
}
