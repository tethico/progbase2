#ifndef EXSISTINGFOLDERCHOICEDIALOG_H
#define EXSISTINGFOLDERCHOICEDIALOG_H

#include <QDialog>

namespace Ui {
class exsistingFolderChoiceDialog;
}

class exsistingFolderChoiceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit exsistingFolderChoiceDialog(QStringList list,QWidget *parent = 0);
    ~exsistingFolderChoiceDialog();
    QString getName();
private slots:
    void on_buttonBox_accepted();

private:
    QString fileName;
    Ui::exsistingFolderChoiceDialog *ui;
};

#endif // EXSISTINGFOLDERCHOICEDIALOG_H
