#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include "stationparser.h"
namespace Ui {
class editDialog;
}

class editDialog : public QDialog
{
    Q_OBJECT

public:
    explicit editDialog(QString name, int numOfExits, double price, QTime interval, QTime timeOpen,QWidget *parent = 0);
    ~editDialog();
    bool bAccepted = false;
    MetroStation getStation();
private slots:
    void on_buttonBox_accepted();

private:
    Ui::editDialog *ui;
    MetroStation station;
};

#endif // EDITDIALOG_H
