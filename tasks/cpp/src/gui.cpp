#include <gui.hh>
#include <iostream>
#include <string>
#include <progbase-cpp/console.h>
#include <assert.h>

Commands GetCommand(int input);
int * GetIntInput(string str);
Point2D *GetVecFromConosle();
void PrintPoints(vector<Point2D> &vec);
void PointAddToVec(vector<Point2D> &vec,Point2D * point);
void PrintAllViableElements(vector<Point2D> &vec,int l);
void systemPause();

int * GetIntInput(string str)
{
    std::cout<< str << ": ";
    int * comm = new int;
    std::cin >> *comm;
    if (cin.fail())
    {
        std::cout<< "Wrong input, value defaulted to 0" << std::endl;
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        *comm = 0;
    }
    return comm;
}

void menu()
{
    std::vector<Point2D> vec;
    bool running = true;
    while (running)
    {
        system("clear");
        cout << "1. Print all points" << endl;
        cout << "2. Add new point" << endl;
        cout << "3. Print points which vector is bigger than num" << endl;
        cout << "4. Exit" << endl;
        switch (GetCommand(*GetIntInput("Enter number of the command")))
        {
        case PrintCollection:
        {
            system("clear");
            PrintPoints(vec);
            systemPause();
            break;
        }
        case AddPointFromConsole:
        {
            system("clear");
            PointAddToVec(vec, GetVecFromConosle());
            break;
        }
        case PrintViablePoints:
        {
            system("clear");
            PrintAllViableElements(vec, *GetIntInput("Enter length"));
            systemPause();
            break;
        }
        case Exit:
        {
            running = false;
            break;
        }
        case NoCommand:
        {
            break;
        }
        }
    }
}

Commands GetCommand(int input)
{
    switch (input)
    {
    case 0:
        return NoCommand;
    case 1:
        return PrintCollection;
    case 2:
        return AddPointFromConsole;
    case 3:
        return PrintViablePoints;
    case 4:
        return Exit;
    default:
        return NoCommand;
    }
}

void PrintPoints(vector<Point2D> &vec)
{
    if(vec.size() == 0){
        cout << "Collection of points is empty" << endl;
    }
    for(int i = 0; i < vec.size();i++)
    {
        cout << "Point №" << i + 1 << endl;
        (vec.at(i)).Print();
        cout << endl;
    }
}

void PointAddToVec(vector<Point2D> &vec,Point2D * point)
{
    vec.push_back(*point);
}

void PrintAllViableElements(vector<Point2D> &vec,int l)
{
    if(vec.size() == 0){
        cout << "Collection of points is empty" << endl;
    }
    for(int i = 0; i < vec.size();i++)
    {
        if((vec.at(i)).GetVectorLength() > l)
        {
            cout << "Point №" << i + 1 << endl;
           (vec.at(i)).Print();
        }
    }
}

Point2D *GetVecFromConosle()
{
    Point2D * p = new Point2D();
    char *name = new char[20];
    cout << "Enter name" << endl;
    cin >> name;
    int * xcor = GetIntInput("Enter x coordinate");
    int * ycor = GetIntInput("Enter y coordinate");
    p->Change_name(name);
    p->Change_x_pos(xcor);
    p->Change_y_pos(ycor);
    return p;
}

void systemPause()
{
    system( "read -n 1 -s -p \"Press any key to continue...\"" );
}