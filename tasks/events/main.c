#include <progbase/events.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>



void ReadFromFile_update(EventHandler *self, Event *event);
void SumNumbers_updater1(EventHandler *self, Event *event);
void SumNumbers_updater2(EventHandler *self, Event *event);
void Text_updater(EventHandler *self, Event *event);
void Sum_mer1(EventHandler *self,Event *event);
void Sum_mer2(EventHandler *self,Event *event);

enum
{
    EventNumberRead,
    UpdateEvent,
    ExitEvent,
    SpecialEventSumOverflow,
    NeedSum1,
    NeedSum2
};

int main()
{
    puts("Обробник 1 по події старту програми зчитує довільний текстовий файл і на зчитування кожної десяткової цифри генерує повідомлення, що містить цифру як дані. Обробник 2 слухає такі події і сумує цифри у число, якщо сума перевищує деяке N - генерується спеціальна подія, що містить цю суму, а сама сума обнуляється. Створити два обробника типу 'Обробник 2', кожен з який при створенні отримує різне значення N.");
    EventSystem_init();
    FILE *f = fopen("txt", "r+");
    EventSystem_addHandler(EventHandler_new(NULL, NULL, Text_updater));
    EventSystem_addHandler(EventHandler_new((FILE *)f, (void *)fclose, ReadFromFile_update));
    int *data = malloc(sizeof(int));
    *data = 5;
    int *data0 = malloc(sizeof(int));
    *data0 = 10; 
    EventSystem_addHandler(EventHandler_new(data0, free, SumNumbers_updater1));
    int *zeroVal = malloc(sizeof(int));
    *zeroVal = 0;
    EventSystem_addHandler(EventHandler_new(zeroVal,free, Sum_mer1));
    EventSystem_addHandler(EventHandler_new(data, free, SumNumbers_updater2));
    int *zeroVal1 = malloc(sizeof(int));
    *zeroVal1 = 0;
    EventSystem_addHandler(EventHandler_new(zeroVal1,free, Sum_mer2));

    EventSystem_loop();

    EventSystem_cleanup();

    return 1;
}

void ReadFromFile_update(EventHandler *self, Event *event)
{

    switch (event->type)
    {
    case StartEventTypeId:
    {
        puts("\nProgram Started\n");
        break;
    }
    case UpdateEventTypeId:
    {

        int *num = malloc(sizeof(int));
        if (fscanf(((FILE *)self->data), " %i", num) != -1)
        {
            EventSystem_emit(Event_new(self, EventNumberRead, num, NULL));
        }
        else
        {
            puts("\nProgram Stopped\n");
            EventSystem_emit(Event_new(self, ExitEvent, NULL, NULL));
        }
        break;
    }
    case ExitEvent:
    {
        EventSystem_exit();
        break;
    }
    }
}

void Text_updater(EventHandler *self, Event *event)
{
    switch (event->type)
    {
    case EventNumberRead:
    {
        printf("I've just read %i from file\n", *((int *)event->data));
        break;
    }
    case SpecialEventSumOverflow:
    {
        printf("Sum overflowed with %i\n",*((int *)event->data));
        *((int *)event->sender->data) = 0;
        break;
    }
    }
}

void SumNumbers_updater1(EventHandler *self, Event *event)
{
    
    switch (event->type)
    {
    case EventNumberRead:
    {
        EventSystem_emit(Event_new(self,NeedSum1,event->data,NULL));
        break;
    }
    }
}
// 
void SumNumbers_updater2(EventHandler *self, Event *event)
{
    switch (event->type)
    {
    case EventNumberRead:
    {
        EventSystem_emit(Event_new(self,NeedSum2,event->data,NULL));
        break;
    }
    }
}

void Sum_mer1(EventHandler *self,Event *event)
{
    switch (event->type)
    {
        case NeedSum1:
        {
            *((int*)self->data) = *((int*)self->data) + *((int*)event->data);
            if(*((int*)self->data) > *((int*)event->sender->data))
            {
                EventSystem_emit(Event_new(self, SpecialEventSumOverflow, self->data, NULL));
            }
            break;
        }
    }
}

void Sum_mer2(EventHandler *self,Event *event)
{
    switch (event->type)
    {
        case NeedSum2:
        {
            *((int*)self->data) = *((int*)self->data) + *((int*)event->data);
            if(*((int*)self->data) > *((int*)event->sender->data))
            {
                EventSystem_emit(Event_new(self, SpecialEventSumOverflow, self->data, NULL));
            }
            break;
        }
    }
}

