#include <bstree.h>
#include <bintree.h>
#include <bintree_ext.h>
#include <stdlib.h>

struct __BSTree {
    BinTree * root;
};


static void BinTree_insert(BinTree * self, int key){   
    if(key < self->value){
        if(self->left == NULL){
            self->left = BinTree_new(key);
        }else{
            BinTree_insert(self->left,key);
        }
    }else{
        if(self->right == NULL){
            self->right = BinTree_new(key);
        }else{
            BinTree_insert(self->right,key);
        }
    }
}

BSTree * BSTree_new(void){
    BSTree *x = malloc(sizeof(BSTree));
    x->root = NULL;
    return x;
}
void BSTree_free(BSTree * self){
    free(self);
}

void BSTree_insert(BSTree * self, int key){
    if(self->root == NULL){
        self->root = BinTree_new(key);
    }else{
        BinTree_insert(self->root,key);
    }
}

static void BinTree_clear(BinTree *self){
    if(self->left != NULL){
        BinTree_clear(self->left);
    }
    if(self->right != NULL){
        BinTree_clear(self->right);
    }
    BinTree_free(self);
}

/**
 *  @brief remove all tree nodes and free their memory
 */
void BSTree_clear(BSTree * self)
{   
    BinTree_clear(self->root);
    BSTree_free(self);
}

// extra

void BSTree_printFormat(BSTree * self){
    BinTree_print(self->root,0);
}

void BSTree_printTraverse(BSTree * self){
    BinTree_inorderPrint(self->root);
}