#pragma once

#include "station.h"
#include "station_string.h"
#include <stdbool.h>
#include <string.h>
#include <csv.h>

#include <station_file.h>
#include <stringbuffer.h>
#include <assert.h>

bool fileExists(const char *fileName);
Station * String_toStationList( char *str);
char *Station_toString( Station * station);
void CsvTable_toStationList(CsvTable * table, List * stations);
int StationList_toCsvTable(char *fileName, List * stations);
CsvTable * CsvTable_newFromFile(char * fileName);