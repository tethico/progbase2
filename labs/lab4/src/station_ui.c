#include "station_ui.h"
#include <assert.h>

Station *inputStruct()
{
    stdin = freopen(NULL, "r", stdin);
    Station *toBeAdded = malloc(sizeof(Station));
    char buf[10];
    bool trueValue = false;
    puts("Enter the name of the station");
    fgets(toBeAdded->name, 20, stdin);
    toBeAdded->name[strlen(toBeAdded->name) - 1] = '\0';
    puts("Enter the price to ride this station");
    while (!trueValue)
    {
        strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
        fgets(buf, 10, stdin);
        trueValue = isDouble(buf);
        if (!trueValue)
            puts("Wrong input");
    }
    toBeAdded->price = strtod(buf, NULL);
    trueValue = false;
    puts("Enter the number of exits this station has");
    while (!trueValue)
    {
        strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
        fgets(buf, 10, stdin);
        trueValue = isInt(buf);
        if (!trueValue)
            puts("Wrong input");
    }
    toBeAdded->numberOfExits = atoi(buf);
    trueValue = false;
    puts("Enter how often trains come here");
    while (!trueValue)
    {
        strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
        fgets(buf, 10, stdin);
        trueValue = isDouble(buf);
        if (!trueValue)
            puts("Wrong input");
    }
    toBeAdded->Timetable.timeBetween = strtod(buf, NULL);
    trueValue = false;
    puts("Enter when station opens");
    while (!trueValue)
    {
        strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
        fgets(buf, 10, stdin);
        trueValue = isDouble(buf);
        if (strtod(buf, NULL) > 24)
        {
            trueValue = false;
        }
        if (!trueValue)
            puts("Wrong input");
    }
    toBeAdded->Timetable.openTime = strtod(buf, NULL);
    trueValue = false;
    return toBeAdded;
}

void menuStart(void)
{
    List *stations = List_new();
    Console_clear();
    puts("1. New list");
    puts("2. Read list from file");
    switch (conGetChar())
    {
    case '1':
        menu(stations);
        break;
    case '2':
        Console_clear();
        puts("Enter the name of .csv file");
        char fileName[20];
        char rFileName[20] = "../";
        bool fExist = false;
        while (!fExist)
        {
            scanf("%s", fileName);
            strcat(rFileName, fileName);
            strcat(rFileName, ".csv");
            fExist = fileExists(rFileName);
            if (strcmp(fileName, "exit\n") == 0)
                break;
            if (!fExist)
            {
                fprintf(stderr, "Error, there's no such file\n");
                abort();
            }
        }
        FILE *f = fopen(rFileName, "r+");
        StringBuffer *sb = StringBuffer_new();
        if (f)
        {
            int res = 1;
            while (res != -1)
            {
                char *buff = malloc(sizeof(char) * 100);
                res = fscanf(f, "%s", buff);
                if (res == -1)
                {
                    buff[0] = '\0';
                    break;
                }
                StringBuffer_append(sb, buff);
                StringBuffer_append(sb, "\n");
            }
            CsvTable_toStationList(CsvTable_newFromString(StringBuffer_toNewString(sb)), stations);
            menu(stations);
        }
        break;
    default:
        break;
    }
}

int DeleteStationStruct_byIndex(int index, List *stations)
{
    int numberOfStations = List_count(stations);
    if (index >= numberOfStations)
    {
        return 0;
    }
    List_removeAt(stations, index);
    return 1;
}

int ChangeStructField(Station *station)
{
    Console_clear();
    puts("1. Change name");
    puts("2. Change price");
    puts("3. Change number of exits");
    puts("4. Change time opens");
    puts("5. Change train periodicity");
    char buf[10];
    char c = conGetChar();
    strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
    bool trueValue = false;
    switch (c)
    {
    case '1': //Change name
        Console_clear();
        puts("Enter the name of the station");
        char buff[20];
        fgets(buff, 20, stdin);
        Console_clear();
        puts("Save station and go back to menu:");
        puts("Press 1 to approve changes. Press any other key to discard the changes");
        c = conGetChar();
        if (c == '1')
        {
            strcpy(station->name, buff);
            station->name[strlen(station->name) - 1] = '\0';
        }

        break;
    case '2': //Change price
        Console_clear();
        puts("Enter the price to ride this station");
        while (!trueValue)
        {
            strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
            fgets(buf, 10, stdin);
            trueValue = isDouble(buf);
            if (!trueValue)
                puts("Wrong input");
        }
        Console_clear();
        puts("Save station and go back to menu:");
        puts("Press 1 to approve changes. Press any other key to discard the changes");
        c = conGetChar();
        if (c == '1')
        {
            station->price = strtod(buf, NULL);
        }

        break;
    case '3': //Change number of exits
        Console_clear();
        puts("Enter the number of exits this station has");
        while (!trueValue)
        {
            strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
            fgets(buf, 10, stdin);
            trueValue = isInt(buf);
            if (!trueValue)
                puts("Wrong input");
        }
        Console_clear();
        puts("Save station and go back to menu:");
        puts("Press 1 to approve changes. Press any other key to discard the changes");
        c = conGetChar();
        if (c == '1')
        {
            station->numberOfExits = atoi(buf);
        }

        break;
    case '4': // change time opens
        Console_clear();
        puts("Enter when station opens");
        while (!trueValue)
        {
            strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
            fgets(buf, 10, stdin);
            trueValue = isDouble(buf);
            if (strtod(buf, NULL) > 24)
            {
                trueValue = false;
            }
            if (!trueValue)
                puts("Wrong input");
        }
        Console_clear();
        puts("Save station and go back to menu:");
        puts("Press 1 to approve changes. Press any other key to discard the changes");
        c = conGetChar();
        if (c == '1')
        {
            station->Timetable.openTime = strtod(buf, NULL);
        }

        break;
    case '5': //change traoin periodicity
        Console_clear();
        puts("Enter how often trains come here");
        while (!trueValue)
        {
            strcpy(buf, "\0\0\0\0\0\0\0\0\0\0");
            fgets(buf, 10, stdin);
            trueValue = isDouble(buf);
            if (!trueValue)
                puts("Wrong input");
        }
        Console_clear();
        puts("Save station and go back to menu:");
        puts("Press 1 to approve changes. Press any other key to discard the changes");
        c = conGetChar();
        if (c == '1')
        {
            station->Timetable.timeBetween = strtod(buf, NULL);
        }

        break;
    default:
        break;
    }
    return 1;
}

void PrintAllStationsOpenedBeforeX(List *stations, int time_x)
{
    List *valStations = List_new();
    int j = 0;
    for (int i = 0; i < List_count(stations); i++)
    {
        Station *buff = malloc(sizeof(Station));
        buff = List_at(stations, i);
        if (buff->Timetable.openTime < time_x)
        {
            List_add(valStations, buff);
        }
    }
    PrintStructArr(valStations);
    List_free(valStations);
}

void PrintStructArr(List *stations)
{
    Station *buff = malloc(sizeof(Station));
    for (int i = 0; i < List_count(stations); i++)
    {
        int j = i * 6;
        buff = List_at(stations, i);
        Console_setCursorPosition(j + 1, 60);
        printf("Name: %s", buff->name);
        Console_setCursorPosition(j + 2, 60);
        printf("Price: %lf", buff->price);
        Console_setCursorPosition(j + 3, 60);
        printf("Number of exits: %i", buff->numberOfExits);
        Console_setCursorPosition(j + 4, 60);
        printf("Time opens at %.2lf o'clock", buff->Timetable.openTime);
        Console_setCursorPosition(j + 5, 60);
        printf("Train comes every %.2lf minutes", buff->Timetable.timeBetween);
        buff = NULL;
    }
    free(buff);
}

void menu(List *stations)
{
    bool running = true;
    while (running)
    {
        Console_clear();
        puts("1. Add station");
        puts("2. Delete station");
        puts("3. Change station");
        puts("4. Change 1 station parameter");
        puts("5. Open all stations that open before certain time");
        puts("6. Save to file");
        puts("7. Return to main menu");
        PrintStructArr(stations);
        char c = conGetChar();
        bool valInt = false;
        switch (c)
        {
        case '1': //add to the end 1 struct
            freopen(NULL, "rw", stdin);
            Console_clear();
            if (List_count(stations) != MAX_NUMBER_OF_STATIONS)
            {
                Station *toAdd = malloc(sizeof(Station));
                toAdd = inputStruct();
                Console_clear();
                puts("Save changes and go back to menu:");
                puts("Press 1 to approve changes. Press any other key to discard the changes");
                c = conGetChar();
                if (c == '1')
                {
                    List_add(stations, toAdd);
                }
            }
            else
                puts("Array is full");
            break;
        case '2': //delete certain struct
            freopen(NULL, "rw", stdin);
            Console_clear();
            int indexToDelete = 0;
            if (List_count(stations) == 0)
            {
                break;
            }
            while (!valInt)
            {
                if (List_count(stations) == 0)
                {

                    puts("List is empty! You cannot do this operation\n");
                    break;
                }
                puts("Enter the index of station that you want to delete");
                char num[10] = "\0\0\0\0\0\0\0\0\0";
                fgets(num, 10, stdin);
                valInt = isInt(num);
                if (valInt)
                {
                    indexToDelete = atoi(num);
                    if (!DeleteStationStruct_byIndex(indexToDelete, stations))
                    {
                        puts("Cannot delete");
                        Console_setCursorPosition(1, 1);
                        puts("            ");
                        Console_setCursorPosition(1, 1);
                        valInt = false;
                    }
                }
            }
            break;
        case '3': //reenter every field of certain struct
            freopen(NULL, "rw", stdin);
            Console_clear();
            int indexToChange = 0;
            while (!valInt)
            {
                if (List_count(stations) == 0)
                {

                    puts("List is empty! You cannot do this operation\n");
                    conGetChar();
                    break;
                }
                puts("Enter the index of station that you want to change");
                char num[10] = "\0\0\0\0\0\0\0\0\0";
                fgets(num, 10, stdin);
                Console_clear();
                if (!valInt)
                {
                    indexToChange = atoi(num);
                    if (indexToChange < List_count(stations))
                    {
                        Station *toAdd = malloc(sizeof(Station));
                        toAdd = inputStruct();
                        Console_clear();
                        puts("Save station and go back to menu:");
                        puts("Press 1 to approve changes. Press any other key to discard the changes");
                        c = conGetChar();
                        if (c == '1')
                        {
                            List_swap(stations, indexToChange, toAdd);
                        }
                        valInt = true;
                    }
                }
            }
            break;
        case '4': //reenter certain field of certain struct
            freopen(NULL, "rw", stdin);
            Console_clear();
            int indexOfStation = 0;
            puts("Enter the index of station that you want to change");
            while (!valInt)
            {
                if (List_count(stations) == 0)
                {

                    puts("List is empty! You cannot do this operation\n");
                    break;
                }
                char num[10] = "\0\0\0\0\0\0\0\0\0";
                fgets(num, 10, stdin);
                valInt = isInt(num);
                if (valInt)
                {
                    indexOfStation = atoi(num);
                    if (indexOfStation < List_count(stations))
                    {
                        Station *buf = List_at(stations, indexOfStation);
                        ChangeStructField(buf);
                        Console_clear();
                        valInt = true;
                    }
                }
            }
            break;
        case '5': //print new array of stations that open before certain time
            freopen(NULL, "rw", stdin);
            Console_clear();
            int timex = 0;
            puts("Enter the time");
            while (!valInt)
            {
                if (List_count(stations) == 0)
                {
                    puts("List is empty! You cannot do this operation\n");
                    break;
                }
                char num[10] = "\0\0\0\0\0\0\0\0\0";
                fgets(num, 10, stdin);
                valInt = isInt(num);
                if (valInt)
                {
                    timex = atoi(num);
                    printf("Stations that open before %i", timex);
                    PrintAllStationsOpenedBeforeX(stations, timex);
                    conGetChar();
                }
            }
            break;
        case '6':
        {
            //save to csv file
            if (List_count(stations) == 0)
            {
                puts("List is empty! You cannot do this operation\n");
                break;
            }
            Console_clear();
            puts("Enter file to write to");
            char fileName[20];
            char rFileName[20] = "../";
            scanf("%s", fileName);
            strcat(rFileName, fileName);
            strcat(rFileName, ".csv");
            puts(rFileName);
            Console_getChar();
            FILE *f = fopen(rFileName, "w+");
            StringBuffer *sb = StringBuffer_new();
            for (int i = 0; i < List_count(stations); i++)
            {
                StringBuffer_append(sb, Station_toString(List_at(stations, i)));
                StringBuffer_append(sb, "\n");
            }
            char *str = StringBuffer_toNewString(sb);
            fputs(str, f);
            fclose(f);
        }
        break;
        case '7':
            List_free(stations);
            menuStart();
            break;
        case 'e':
            running = false;
            List_free(stations);
            break;
        default:
            break;
        }
    }
}