#include <csv.h>
#include <stringbuffer.h>
#include <assert.h>

struct __CsvTable
{
    List *rows;
};
struct __CsvRow
{
    List *values;
};

char *ft_strdup(char *src)
{
    char *str;
    char *p;
    int len = 0;

    while (src[len])
        len++;
    str = malloc(len + 1);
    p = str;
    while (*src)
        *p++ = *src++;
    *p = '\0';
    return str;
}

static void List_copyFrom(List *self, List *from)
{
    int count = (int)List_count(from);
    for (int i = 0; i < count; i++)
    {
        List_add(self, List_at(from, i));
    }
}

CsvRow *CsvRow_new(void)
{
    CsvRow *self = malloc(sizeof(CsvRow));
    self->values = List_new();
    return self;
}

void CsvRow_free(CsvRow *self)
{
    List_free(self->values);
    free(self);
}

void CsvRow_add(CsvRow *self, const char *value)
{
    List_add(self->values, (void *)ft_strdup((char *)value));
}

void CsvTable_values(CsvRow *self, List *values)
{
    List_copyFrom(values, self->values);
}

CsvTable *CsvTable_new(void)
{
    CsvTable *self = malloc(sizeof(CsvTable));
    self->rows = List_new();
    return self;
}
void CsvTable_free(CsvTable *self)
{
    int size = List_count(self->rows);
    for (int i = 0; i < size; i++)
    {
        CsvRow *toFree = List_at(self->rows, i);
        int size1 = List_count(toFree->values);
        for (int j = 0; j < size1; j++)
        {
            free(List_at(toFree->values, j));
        }
        CsvRow_free(toFree);
    }
    List_free(self->rows);
    free(self);
}
void CsvTable_add(CsvTable *self, CsvRow *row)
{
    List_add(self->rows, row);
}
void CsvTable_rows(CsvTable *self, List *rows)
{
    List_copyFrom(rows, self->rows);
}

static char *copyNewValueFromBufferAndClear(StringBuffer *sb)
{
    char *value = StringBuffer_toNewString(sb);
    char *copy = ft_strdup(value);
    StringBuffer_clear(sb);
    return copy;
}

List * CsvRowValues(CsvRow* self){
    List * vals = List_new();
    for(int i = 0; i < List_count(self->values);i++)
    {   
        List_add(vals,&self->values[i]);
    }
}

CsvTable *CsvTable_newFromString(const char *csvString)
{
    CsvTable *table = CsvTable_new();
    CsvRow *row = CsvRow_new();
    StringBuffer *sb = StringBuffer_new();
    const char *input = csvString;
    while (1)
    {
        if (*input == '\0')
        {
            List_add(row->values, (void *)copyNewValueFromBufferAndClear(sb));
            CsvTable_add(table, row);
            break;
        }
        else if (*input == ',')
        {
            List_add(row->values, (void *)copyNewValueFromBufferAndClear(sb));
        }
        else if (*input == '\n')
        {
            List_add(row->values, (void *)copyNewValueFromBufferAndClear(sb));
            CsvTable_add(table, row);
            row = CsvRow_new();
        }
        else
        {
            StringBuffer_appendChar(sb, *input);
        }
        input++;
    }
    StringBuffer_free(sb);
    return table;
}
char *CsvTable_toNewString(CsvTable *self)
{
    StringBuffer *sb = StringBuffer_new();
    int nRows = List_count(self->rows);
    for (int i = 0; i < nRows; i++)
    {
        CsvRow *row = List_at(self->rows, i);
        if (i != 0)
            StringBuffer_append(sb, "\n");
        int values = List_count(row->values);
        for (int j = 0; j < values; j++)
        {
            char *value = List_at(row->values, j);
            if (j != 0)
            {
                StringBuffer_append(sb, ",");
            }

            StringBuffer_append(sb, value);
        }
    }
    char *a = ft_strdup(StringBuffer_toNewString(sb));
    StringBuffer_free(sb);
    return a;
}