#pragma once

#include <string>
#include <vector>
#include <scrummaster.h>

class EntityStorage {
protected:
     std::string _name;
     EntityStorage(std::string & name) { this->_name = name; }
public:
     std::string & name() { return this->_name; }

     virtual std::vector<ScrumMaster> load() = 0;
     virtual void save(std::vector<ScrumMaster> & entities) = 0;
};
