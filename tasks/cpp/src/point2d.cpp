#include <point2d.hh>
#include <math.h>
#include <iostream>
#include <string>

void Point2D::Change_x_pos(int *value)
{
    this->x_pos = value;
}

void Point2D::Change_y_pos(int *value)
{
    this->y_pos = value;
}

void Point2D::Change_name(char * value)
{
    this->name = value;
}

int Point2D::GetVectorLength()
{
    return sqrt(pow(*((int*)this->x_pos),2) + pow(*((int*)this->y_pos), 2));
}

void Point2D::Print()
{
    std::cout << "Name of the Point - "<< this->name << std::endl << "Coordinate X = "<< *((int*)this->x_pos) << std::endl << "Coordinate Y = "<<  *((int*)this->y_pos) << std::endl;
}