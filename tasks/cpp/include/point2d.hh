#pragma once

class Point2D{
    private:
        char * name;
        int * x_pos;
        int * y_pos;
    public:
        void Change_name(char * value);
        void Change_x_pos(int * value);
        void Change_y_pos(int * value);
        int GetVectorLength();
        void Print();
};