#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTcpSocket>
#include <QMessageBox>
#include <QTimer>

void errorWindowInit(QString errMsg){
    QMessageBox *mbox = new QMessageBox;
    mbox->setWindowTitle(("Error"));
    mbox->setText(errMsg);
    mbox->exec();
    mbox->setStandardButtons(QMessageBox::Ok);
    QTimer::singleShot(5000, mbox, SLOT(hide()));
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->objInfo->hide();
    ui->editBtn->setDisabled(true);
    ui->delBtn->setDisabled(true);
    connDial = new connDialog();
    connDial->exec();
    if(connDial->bSuccessfulConn)
    {
        this->address = connDial->getAddress();
        this->port = connDial->getPort();
        bConnected = true;
    }else{
        this->setEnabled(false);
        errorWindowInit("You are not connected to the server, please restart");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onReadyRead()
{
    QTcpSocket * socket = static_cast<QTcpSocket*>(sender());
    //get responses
    //disconnect from host
    qDebug() << "ready for read";
        QByteArray cmnd = socket->read(1);
        QByteArray bytes = socket->readAll();
        switch(cmnd.toInt())
        {
        case 1:
        {
            FillList(jsonFmt.listFromJson(bytes));
            break;
        }
        case 2:
        {
            QString listOfFiles = bytes;
            list = listOfFiles.split(",");
            qDebug() << listOfFiles;
        }
        }
    request = "";
    socket->disconnectFromHost();
}

void MainWindow::on_addBtn_clicked()
{
    //add dial exec
    //send req to add MS
    QTcpSocket * socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),SLOT(onConnected()));
    connect(socket,SIGNAL(readyRead()),SLOT(onReadyRead()));
    request = "2";
    addDial = new addDialog();
    addDial->exec();
    if(addDial->bAccepted){
        request.append(jsonFmt.MStoJson(addDial->getStation()));
    }else{
        request = "";
    }
    qDebug() << request;
    socket->connectToHost(address,port);
    socket->waitForConnected(3000);
    socket->write(request.toUtf8());
    socket->flush();
}

void MainWindow::on_delBtn_clicked()
{
    //send req to delete MS
    QTcpSocket * socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),SLOT(onConnected()));
    connect(socket,SIGNAL(readyRead()),SLOT(onReadyRead()));
    request = "3";           //add one more number in request for index to delete
    QListWidgetItem *item = ui->listWidget->selectedItems().at(0);
    int row = ui->listWidget->row(item);
    request.append(QString::number(row));
    qDebug() << request;
    socket->connectToHost(address,port);
    socket->waitForConnected(3000);
    socket->write(request.toUtf8());
    socket->flush();

}

void MainWindow::on_editBtn_clicked()
{
    //edit dial exec
    //send req to replace MS
    QTcpSocket * socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),SLOT(onConnected()));
    connect(socket,SIGNAL(readyRead()),SLOT(onReadyRead()));
    request = "4";
    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    QVariant var = item->data(Qt::UserRole);
    int index = ui->listWidget->row(item);
    MetroStation ms = var.value<MetroStation>();
    editDial = new editDialog(ms.gname(),ms.gnumOfExits(),ms.gprice(),ms.gtimetable().ginterval(),ms.gtimetable().gtimeOpens());
    editDial->exec();
    if(editDial->bAccepted){
        request.append(QString::number(index));
        request.append(jsonFmt.MStoJson(editDial->getStation()));
    }else{
        request = "";
    }
    qDebug() << request;
    socket->connectToHost(address,port);
    socket->waitForConnected(3000);
    socket->write(request.toUtf8());
    socket->flush();

}

void MainWindow::on_actionSave_triggered()
{
    //open dialog enter filename
    //send req to save to file
    QTcpSocket *socket = new QTcpSocket();
    if(bConnected)
    {
        connect(socket,SIGNAL(connected()),SLOT(onConnected()));
        connect(socket,SIGNAL(readyRead()),SLOT(onReadyRead()));
        socket->connectToHost(address,port);
        socket->waitForConnected(3000);
        socket->write("0");
        socket->flush();
        socket->waitForReadyRead(2000);
        exsistfileChoiceDial = new exsistingFolderChoiceDialog(list);
        exsistfileChoiceDial->exec();
        socket->connectToHost(address,port);
        socket->waitForConnected(2000);
        qDebug() << "writing name";
        QString data = "6" + exsistfileChoiceDial->getName();
        socket->write(data.toUtf8());
        socket->flush();
    }else{
        errorWindowInit("You are not connected to the server, please restart");
    }

}

void MainWindow::on_actionLoad_triggered()
{
    //open dialog to choose file from /data dir
    //send req to save from that file
    if(bConnected)
    {
        QTcpSocket *socket = new QTcpSocket();
        connect(socket,SIGNAL(connected()),SLOT(onConnected()));
        connect(socket,SIGNAL(readyRead()),SLOT(onReadyRead()));
        socket->connectToHost(address,port);
        socket->waitForConnected(3000);
        socket->write("0");
        socket->flush();
        socket->waitForReadyRead(2000);
        exsistfileChoiceDial = new exsistingFolderChoiceDialog(list);
        exsistfileChoiceDial->exec();
        socket->connectToHost(address,port);
        socket->waitForConnected(2000);
        qDebug() << "writing name";
        QString data = "7" + exsistfileChoiceDial->getName();
        socket->write(data.toUtf8());
        socket->flush();
    }else{
        errorWindowInit("You are not connected to the server, please restart");
    }
}

void MainWindow::on_actionNew_file_triggered()
{
    //open dialog to enter new filename
    // create new file in /data dir
    QTcpSocket * socket = new QTcpSocket();
    connect(socket,SIGNAL(connected()),SLOT(onConnected()));
    request = "1";
    fileChoiceDial = new fileChoiceDialog();
    fileChoiceDial->exec();
    if(fileChoiceDial->bAccepted){
        request.append(fileChoiceDial->getFileName());
    }else{
        request = "";
    }
    qDebug() << request;
    socket->connectToHost(address,port);
    socket->waitForConnected(3000);
    socket->write(request.toUtf8());
    socket->flush();
}

void MainWindow::onConnected()
{

}

void MainWindow::on_listWidget_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItems = ui->listWidget->selectedItems();
    if(selectedItems.count() == 0)
    {
        //remove description
        ui->editBtn->setEnabled(false);
        ui->delBtn->setEnabled(false);
        ui->objInfo->hide();

    }else{
        //change description
        ui->editBtn->setEnabled(true);
        ui->delBtn->setEnabled(true);
        ui->objInfo->show();
        MetroStation st = qvariant_cast<MetroStation>(selectedItems.at(0)->data(Qt::UserRole));
        ui->nameField->setText(st.gname());
        ui->numOfExField->setText(QString::number(st.gnumOfExits()));
        ui->priceField->setText(QString::number(st.gprice()));
        ui->timeOpenField->setText(st.gtimetable().gtimeOpens().toString("hh:mm"));
        ui->intervalField->setText(st.gtimetable().ginterval().toString("hh:mm"));
    }
}

void MainWindow::FillList(QList<MetroStation> vec)
{
    ui->listWidget->clear();
    for(uint i = 0; i < (uint)vec.size();i++){
        MetroStation station = vec.at(i);
        QVariant qvar;
        qvar.setValue(station);
        QListWidgetItem* metroItem = new QListWidgetItem();
        metroItem->setText(station.gname());
        metroItem->setData(Qt::UserRole,qvar);
        ui->listWidget->addItem(metroItem);
    }
}
