#pragma once

#include <list.h>
#include <stdlib.h>

#define MAX_NUMBER_OF_STATIONS 10

struct TrainTimetable
{
    double timeBetween;
    double openTime;
};

typedef struct Station
{
    char name[20];
    int numberOfExits;
    double price;
    struct TrainTimetable Timetable;
} Station;

int NumOfStationStructs(Station *stations[MAX_NUMBER_OF_STATIONS]);



