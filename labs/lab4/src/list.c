#include <stdlib.h>
#include <list.h>
#include <assert.h>
#include <stdio.h>


struct List {
    void ** items;
    int capaicty;
    int length;
};

static const int INITIAL_CAPASITY = 16;

List *List_new(void)
{
    List *self = malloc(sizeof(List));
    self->capaicty = INITIAL_CAPASITY;
    self->length = 0;
    self->items = malloc(sizeof(void *) * INITIAL_CAPASITY);
    return self;
}

void List_add(List *self, void *value)
{
    if (self->length == INITIAL_CAPASITY)
    {
        self->capaicty = self->capaicty * 2;
    }
    self->items = realloc(self->items, sizeof(int) * self->capaicty);
    self->items[self->length] = value;
    self->length++;
}

void List_free(List *self)
{
    free(self->items);
    free(self);
}

void *List_at(List *self, size_t index)
{
    if (index > self->length)
    {
        printf("Error, index is bigger than lentgh");
        abort();
    }
    return self->items[index];
}

void List_removeAt(List *self, size_t index)
{
    if (index > self->length)
    {
        printf("Error, index is bigger than lentgh");
        abort();
    }
    for (int i = index; i < self->length; i++)
    {
        self->items[i] = self->items[i + 1];
    }
    self->length--;
}

void List_insert(List *self, size_t index, void *value)
{
    if (self->length == 0)
    {
        List_add(self, value);
    }
    else
    {
        if (index > self->length)
        {
            printf("Error, index is bigger than lentgh");
            abort();
        }
        if (self->length == self->capaicty)
        {
            self->items = realloc(self->items, sizeof(self) + sizeof(int));
        }
        int i = self->length;
        for (; i > self->length - index; i--)
        {
            self->items[i + 1] = self->items[i];
        }
        self->items[i] = value;
        self->length++;
    }
}

size_t List_count(List *self)
{
    return self->length;
}

void List_swap(List *self, size_t index, void* value)
{
    if(self->length < index)
    {
        fprintf(stderr,"Error, index is bigger than length");
        abort();
    }
    self->items[index] = value; 
}