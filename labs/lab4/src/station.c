#include "station.h"

int NumOfStationStructs(Station *stations[MAX_NUMBER_OF_STATIONS])
{
    for (int i = 1; i <= MAX_NUMBER_OF_STATIONS; i++)
    {
        if (stations[i - 1] == NULL)
        {
            return i - 1;
        }
    }
    return -1;
}