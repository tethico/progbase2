#pragma once

#include "entitystorage.h"

class JsonEntityStorage : public EntityStorage
{
public:
    JsonEntityStorage(std::string name);
    std::vector<ScrumMaster> load();
    void save(std::vector<ScrumMaster> & entities);
};
