#pragma once

#include <QMainWindow>
#include <QHostAddress>
#include <QTcpSocket>
#include "stationparser.h"
#include "adddialog.h"
#include "editdialog.h"
#include "filechoicedialog.h"
#include "conndialog.h"
#include "exsistingfolderchoicedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
void FillList(QList<MetroStation> vec);
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void onReadyRead();
private slots:
    void on_addBtn_clicked();

    void on_delBtn_clicked();

    void on_editBtn_clicked();

    void on_actionSave_triggered();

    void on_actionLoad_triggered();

    void on_actionNew_file_triggered();

    void onConnected();
    void on_listWidget_itemSelectionChanged();

private:
    QStringList list;
    bool bConnected;
    exsistingFolderChoiceDialog* exsistfileChoiceDial;
    connDialog* connDial;
    addDialog* addDial;
    editDialog* editDial;
    fileChoiceDialog* fileChoiceDial;
    StationParser jsonFmt;
    QHostAddress address = QHostAddress("127.0.0.1");
    quint16 port = 3000;
    QString request;
    Ui::MainWindow *ui;
};


