#include <lexer.h>
#include <stdio.h>
#include <list.h>
#include <assert.h>
#include <parser.h>
#include <ast.h>
#include <ast_ext.h>
#include <interpreter.h>

int main(void)
{
    FILE *f = fopen("../main.zkarl","r+");
    assert(f != NULL);//file doesnt exist
    char str[100];
    while(fread(str,1,100,f));
    // read file content
    fclose(f);
    List *tokens = List_new();
    if (0 != Lexer_splitTokens(str, tokens))
    {
        assert(0);
        Lexer_clearTokens(tokens);
        List_free(tokens);
        return EXIT_FAILURE;
    }
    Lexer_printTokens(tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    //int runStatus = Interpreter_execute(root);
    puts("\n_________________\n");
    AstTree_print(root);
    puts("\n_________________\n");
    Lexer_clearTokens(tokens);
    Tree_free(root);
    List_free(tokens);
    return EXIT_SUCCESS;
}
