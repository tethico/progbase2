#include "xmlentitystorage.h"
#include <QtXml>
#include <iostream>
#include <fstream>

XmlEntityStorage::XmlEntityStorage(std::string name) : EntityStorage(name){
    this->_name = name;
}

std::vector<ScrumMaster> XmlEntityStorage::load()
{
        std::vector<ScrumMaster> toRet;
        std::string str;
        str = "../json_xml/" + this->_name;
        std::ifstream file;
        file.open(str);
        if (!file) {
                std::cout << "Unable to open file in method";
                return toRet;
            }
        std::string buff;
        std::string docString;
        while(file.good()){
            std::getline(file,buff,'\0');
            docString.append(buff);
        }
        QDomDocument doc;
        QDomProcessingInstruction instr = doc.createProcessingInstruction(
                             "xml", "version='1.0' encoding='UTF-8'");
           doc.appendChild(instr);
        if (!doc.setContent(QString::fromStdString(docString))) {
           std::cerr << "error parsing xml!" << std::endl;
           //todo throw exep
                    return toRet;
           }
           QDomElement root = doc.documentElement();
              for (int i = 0; i < root.childNodes().length(); i++) {
                    QDomNode SMNode = root.childNodes().at(i);
                    QDomElement smasterEl = SMNode.toElement();
                    ScrumMaster smaster;
                    smaster.name = smasterEl.attribute("name").toStdString();
                    smaster.age = smasterEl.attribute("age").toInt();
                    smaster.efficencyMark = smasterEl.attribute("efficencyMark").toDouble();
                    QDomElement team = smasterEl.firstChild().toElement();
                    for(int j = 0; j < team.childNodes().count(); j++){
                        smaster.team.push_back(team.childNodes().at(j).toElement().attribute("name").toStdString());
                    }
                    toRet.push_back(smaster);
              }
              return toRet;
}

void XmlEntityStorage::save(std::vector<ScrumMaster> &entities)
{
    QDomDocument doc;
    QDomElement array = doc.createElement("ScrumMasters");
    for(unsigned int i = 0; i < entities.size(); i++)
    {
        QDomElement master = doc.createElement("ScrumMaster");
        master.setAttribute("age",entities.at(i).age);
        master.setAttribute("name",entities.at(i).name.c_str());
        master.setAttribute("efficencyMark",entities.at(i).efficencyMark);
        QDomElement team = doc.createElement("team");
        for(unsigned int j = 0;j < entities.at(i).team.size();j++)
        {
            QDomElement member = doc.createElement("member");
            member.setAttribute("name",entities.at(i).team[j].c_str());
            team.appendChild(member);
        }
        master.appendChild(team);
        array.appendChild(master);
    }
    doc.appendChild(array);
    std::string str;
    str = "../json_xml/" + this->_name;
    FILE * f = fopen(str.c_str(),"w+");
    fprintf(f,"%s",doc.toString().toStdString().c_str());
    fclose(f);
}
