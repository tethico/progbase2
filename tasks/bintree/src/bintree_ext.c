#include <stdio.h>
#include <bintree_ext.h>

void BinTree_print(BinTree* self, int depth){
    for(int i = 0; i < depth;i++){
            printf("..");
    }
    if(self == NULL){
            printf("(null)\n");
    }else{
            printf("%i\n",self->value);
            depth++;
            if(self->left != NULL || self->right != NULL){
                BinTree_print(self->left,depth);
                BinTree_print(self->right,depth);
            }
    }
}

void BinTree_inorderPrint(BinTree* self){
    if(self->left != NULL)
        BinTree_inorderPrint(self->left);
    printf("%i ",self->value);
    if(self->right != NULL)
        BinTree_inorderPrint(self->right);
}