#pragma once
#include <stdlib.h>

typedef struct __NumList NumList;

struct __NumList {
    int * items;
    int capaicty;
    int length;
};

NumList * Numlist_new(void);
void Numlist_free(NumList * self);

void Numlist_add(NumList * self, int value);
void Numlist_insert(NumList * self, size_t index, int value);
int Numlist_at(NumList * self, size_t index);
int Numlist_set(NumList * self, size_t index, int value);
int Numlist_removeAt(NumList * self, size_t index);
size_t Numlist_count(NumList * self);