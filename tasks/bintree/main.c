#include <stdio.h>
#include <stdlib.h>
#include <bintree.h>
#include <bintree_ext.h>
#include <bstree.h>


int main(){
    int arr [] = {10, 8, 11, 16, 17, 9, -8};
    BSTree * tree = BSTree_new();
    for(int i = 0; i < sizeof(arr)/sizeof(int); i++){
        BSTree_insert(tree,arr[i]);       
    }
    BSTree_printFormat(tree);
    printf("\n");
    BSTree_printTraverse(tree);
    BSTree_clear(tree);
}