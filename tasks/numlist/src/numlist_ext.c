#include <numlist_ext.h>
#include <stdio.h>

void Numlist_print(NumList *self)
{
    puts("");
    for(int i = 0; i < self->length;i++)
    {
        printf(" %3i ",self->items[i]);
    }
    puts("");
}
void Numlist_deleteNegative(NumList* self)
{
    for(int i = 0; i < self->length;i++)
    {

        while(self->items[i] < 0)
        {
            Numlist_removeAt(self,i);
        }
    }
}

