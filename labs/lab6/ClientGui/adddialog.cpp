#include "adddialog.h"
#include "ui_adddialog.h"

addDialog::addDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addDialog)
{
    ui->setupUi(this);
}

addDialog::~addDialog()
{
    delete ui;
}

MetroStation addDialog::getStation()
{
    return station;
}

void addDialog::on_buttonBox_accepted()
{
    bAccepted = true;
    station.setName(ui->lineEdit->text());
    station.setNumOfExits(ui->spinBox->value());
    station.setPrice(ui->doubleSpinBox->value());
    station.setTimetable(ui->timeEdit_2->time(),ui->timeEdit->time());
}
