#pragma once

#include "entitystorage.h"

class XmlEntityStorage : public EntityStorage
{
public:
    XmlEntityStorage(std::string name);
    std::vector<ScrumMaster> load();
    void save(std::vector<ScrumMaster> & entities);
};
