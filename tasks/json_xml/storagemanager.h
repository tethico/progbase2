#pragma once

#include <entitystorage.h>

class StorageManager
{
    public:
        static EntityStorage * createStorage(std::string storageFileName);
};
