#include "addstationdialog.h"
#include "ui_addstationdialog.h"

addStationDialog::addStationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addStationDialog)
{
    ui->setupUi(this);
}

addStationDialog::~addStationDialog()
{
    delete ui;
}
