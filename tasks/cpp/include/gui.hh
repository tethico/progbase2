#pragma once

#include <point2d.hh>
#include <vector>

enum Commands{
    NoCommand,
    PrintCollection,
    AddPointFromConsole,
    PrintViablePoints,
    Exit
};

using namespace std;


void PrintPoints(vector<Point2D> &vec);
void PointAddToVec(vector<Point2D> &vec,Point2D * point);
void PrintAllViableElements(vector<Point2D> &vec,int l);
void ExitMenu();
void menu();

