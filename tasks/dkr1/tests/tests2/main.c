#include <list.h>
#include <lexer.h>
#include <check.h>
#include <parser.h>
#include <stdio.h>
#include <ast.h>

START_TEST (TreeCreate)
{
    int * xValPtr = malloc(sizeof(int));
    *xValPtr = 10;
    Tree * x = Tree_new(xValPtr);
    ck_assert_int_eq(*((int *)x->value),10); 
    free(xValPtr);
    Tree_free(x);
}
END_TEST

START_TEST (TreeAddChildren)
{
    int * someValPtr1 = malloc(sizeof(int));
    *someValPtr1 = 10;
    int * someValPtr2 = malloc(sizeof(int));
    *someValPtr2 = 50;
    int * someValPtr3 = malloc(sizeof(int));
    *someValPtr3 = 30;
    Tree * x = Tree_new(someValPtr1);
    List_add(x->children,someValPtr2);
    List_add(x->children,someValPtr3); 
    ck_assert_int_eq(*((int *)x->value),10); 
    ck_assert_int_eq(*((int*)List_at(x->children,0)),50); 
    ck_assert_int_eq(*((int*)List_at(x->children,1)),30); 
    free(someValPtr1);
    free(someValPtr2);
    free(someValPtr3);
    Tree_free(x);
}
END_TEST

START_TEST (ParserBuildTreeFrom1Number)
{
    List * tok = List_new();
    Token * t1 = Token_new(TokenType_NUMBER,"10.0");
    Token * t2 = Token_new(TokenType_TERMINATOR,"");
    List_add(tok,t1);
    List_add(tok,t2);
    Tree * tree = Parser_buildNewAstTree(tok);
    ck_assert_str_eq(((Token*)tree->value)->name,"program");
    ck_assert_int_eq(List_count(tree->children),1);
    Token_free(t1);
    Token_free(t2);
    List_free(tok);
    Tree_free(tree);
}
END_TEST


Suite *test_suite(void) {
  Suite *s = suite_create("Module");
  TCase *tc_sample;
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tcase_add_test(tc_sample, TreeAddChildren);
  tcase_add_test(tc_sample, TreeCreate);
  tcase_add_test(tc_sample,ParserBuildTreeFromNumber);
  suite_add_tcase(s, tc_sample);

  return s;
}

int main(void) {
  Suite *s = test_suite();
  SRunner *sr = srunner_create(s);
  srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

  srunner_run_all(sr, CK_VERBOSE);

  int num_tests_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return num_tests_failed;
}

