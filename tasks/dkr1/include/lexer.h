#pragma once
#include <list.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stringbuffer.h>


typedef enum {
   TokenType_COMMA,
   TokenType_PLUS,
   TokenType_MINUS,
   TokenType_MULT,
   TokenType_DIV,
   TokenType_POW,
   TokenType_ASSIGN,
   TokenType_MOD,
   //
   TokenType_WHILE,
   TokenType_IF,
   TokenType_ELSE,
   TokenType_BRACKET_L,
   TokenType_BRACKET_R,
   TokenType_BLOCK_START,
   TokenType_BLOCK_END,
   //
   TokenType_LOG_NOT,
   TokenType_LOG_EQ,
   TokenType_LOG_NEQ,
   TokenType_LOG_AND,
   TokenType_LOG_OR,
   TokenType_LOG_LESS,
   TokenType_LOG_MORE,
   TokenType_LOG_LESSEQ,
   TokenType_LOG_MOREEQ,
   //
   TokenType_BOOL,
   TokenType_STRING,
   TokenType_NUMBER,
   TokenType_VAR,
   TokenType_ARRAY,
   TokenType_NAME,
   TokenType_TERMINATOR,
   TokenType_UNK
} TokenType;

typedef struct __Token Token;

struct __Token {
   TokenType type;
   const char * name;
};


const char * TokenType_toString(TokenType tokenType);
Token * Token_new(TokenType type, const char * name);
void Token_free(Token * self);

bool Token_equals(Token * self, Token * other);

void TokenType_print(TokenType type);

int Lexer_splitTokens(const char * input, List * tokens);

void Lexer_clearTokens(List * tokens);

void Lexer_printTokens(List * tokens);
