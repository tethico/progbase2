#pragma once

#include <list.h>
#include "station.h"
#include <stdio.h>
#include <stdlib.h>

void StationStruct_toString(char *finString, Station *station);
Station *String_toStationStruct(char *srcString);