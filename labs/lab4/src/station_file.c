#include <station_file.h>
#include <stringbuffer.h>
#include <assert.h>

static int DEFAULT_BUFFER_CAPACITY = 100;

bool fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return false; //not exists
    fclose(f);
    return true; //exists
}

Station * CsvRow_toStation( char *str)
{
    Station * ret = malloc(sizeof(Station));
    char *buff = malloc(strlen(str)*sizeof(char)); 
    bool nameRead = false;
    for(int i = 0;i < strlen(str);i++)
    {
        if(str[i] == ',' )
        {
            if(nameRead)
            {
                str[i] = ' ';
            }else{
                nameRead = true;
            }
        }
    }
    int nmRed = sscanf(str,"%20[^,]%*c",ret->name);
    if(nmRed == 0){
        strcpy(ret->name,"");
    }
    int nameLen = 0;
    if(nmRed)
        nameLen = strlen(ret->name);
    sscanf(str +nameLen + 1,"%lf %i %lf %lf",&ret->price,&ret->numberOfExits,&ret->Timetable.timeBetween,&ret->Timetable.openTime);
    List * r = List_new();
    List_add(r,ret);
    return ret;
}

char *Station_toString( Station * station)
{
    //TODO
    char * str = malloc(sizeof(char)*DEFAULT_BUFFER_CAPACITY);
    sprintf(str,"%s,%f,%i,%f,%f",station->name,station->price,station->numberOfExits,station->Timetable.timeBetween,station->Timetable.openTime);
    return str;
}

void CsvTable_toStationList(CsvTable *table, List * stations) 
{
    char * str = CsvTable_toNewString(table);
    int res = 1;
    while(res){
        Station * buf = malloc(sizeof(Station));
        char * buffer = malloc(sizeof(char)*100);
        res = sscanf(str,"%[^\n]s",buffer);
        int bfLen = 0;
        if(res == 1)
            bfLen = strlen(buffer);
        str = str + bfLen + 1; 
        if (res == -1)
            break;
        List_add(stations,CsvRow_toStation(buffer));
    }
}

int StationList_toCsvTable(char *fileName, List * stations)
{
    FILE *f = fopen(fileName, "w+");
    if (!f){
        return 0;
        fprintf(stderr,"IDK really");
    }
    fclose(f);
    return 1;
}

CsvTable * CsvTable_newFromFile(char * fileName)
{
    FILE *f = fopen(fileName,"r+");
    if(!f)
        return 0;
    StringBuffer *sb = StringBuffer_new();
    while(true){
        char buf [DEFAULT_BUFFER_CAPACITY];
        if(fgets(buf,DEFAULT_BUFFER_CAPACITY,f))
            StringBuffer_append(sb,buf);
        else
            break;
    }
}