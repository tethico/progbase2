#pragma once

#include <stdlib.h>

#define foreach(VARTYPE, VARNAME, LIST)   \
   for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
       for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
           VARNAME##_i < VARNAME##_length; \
           VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))

typedef struct __List List;

List * List_new(void);
void List_free(List * self);

void List_add(List * self, void* value);
void List_swap(List *self, size_t index, void* value);
void List_insert(List * self, size_t index, void* value);
void* List_at(List * self, size_t index);
void List_removeAt(List * self, size_t index);
size_t List_count(List * self);