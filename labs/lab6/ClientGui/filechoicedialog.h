#ifndef FILECHOICEDIALOG_H
#define FILECHOICEDIALOG_H

#include <QDialog>

namespace Ui {
class fileChoiceDialog;
}

class fileChoiceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit fileChoiceDialog(QWidget *parent = 0);
    ~fileChoiceDialog();
    QString getFileName();
    bool bAccepted = false;
private slots:
    void on_buttonBox_accepted();

private:
    QString fileName;
    Ui::fileChoiceDialog *ui;
};

#endif // FILECHOICEDIALOG_H
