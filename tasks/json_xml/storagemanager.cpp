#include "storagemanager.h"
#include "xmlentitystorage.h"
#include "jsonentitystorage.h"
#include "messageexception.h"

EntityStorage * StorageManager::createStorage(std::string storageFileName)
{
    if(strlen(storageFileName.c_str()) > 5){
        if(strcmp(storageFileName.c_str() + strlen(storageFileName.c_str()) - 4,".xml") == 0){
            //read xml
            XmlEntityStorage * manager = new XmlEntityStorage(storageFileName);
            return manager;
        }else if(strcmp(storageFileName.c_str() + strlen(storageFileName.c_str()) - 5,".json") == 0){
            //read json
            JsonEntityStorage * manager = new JsonEntityStorage(storageFileName);
            return manager;
        }else{
            // throw exeption
            std::string error = "Cannot create store manager from invalid file format :";
            error.append(storageFileName);
            MessageException err = MessageException(error);
            throw err;
        }
    }else{
        // throw exeption
        std::string error = "Cannot create store manager from invalid file format :";
        error.append(storageFileName);
        MessageException err = MessageException(error);
        throw err;
    }
}
