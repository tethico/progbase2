#include "scrummaster.h"

ScrumMaster::ScrumMaster(std::string name, int age, double efficencyMark, std::vector<std::string> names)
{
    this->name = name;
    this->age = age;
    this->efficencyMark = efficencyMark;
    this->team = names;
}

ScrumMaster::ScrumMaster()
{
    this->name = "";
    this->age = 0;
    this->efficencyMark = 0;
    this->team = std::vector<std::string>();
}
