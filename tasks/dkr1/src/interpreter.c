#include <interpreter.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ast.h>
#include <math.h>
#include <string.h>

typedef enum {
    ValueType_NUMBER,
    ValueType_BOOL,
    ValueType_STRING,
    ValueType_UNDEF,
    ValueType_ARR
} ValueType;

typedef struct __Value Value;
struct __Value
{
    ValueType type;
    void *value;
};

typedef struct __Var Var;
struct __Var
{
    const char *name;
    Value *value;
};

Value *Value_new(ValueType type, void *value)
{
    Value *self = malloc(sizeof(Value));
    self->type = type;
    self->value = value;
    return self;
}
void Value_free(Value *self)
{
    free(self->value);
    free(self);
}

void ValueType_print(ValueType self)
{
    switch (self)
    {
    case ValueType_NUMBER:
        printf("number");
        break;
    case ValueType_STRING:
        printf("string");
        break;
    case ValueType_BOOL:
        printf("boolean");
        break;
    case ValueType_ARR:
        printf("array");
        break;
    case ValueType_UNDEF:
        printf("undefined");
        break;
    default:
        printf("<?>");
        break;
    }
}
void Value_print(Value *self)
{
    switch (self->type)
    {
    case ValueType_NUMBER:
    {
        double val = *(double *)self->value;
        printf("%lf | ", val);
        break;
    }
    case ValueType_STRING:
    {
        char *val = (char *)self->value;
        printf("\"%s\" | ", val);
        break;
    }
    case ValueType_BOOL:
    {
        bool val = *(bool *)self->value;
        printf("%s | ", val ? "true" : "false");
        break;
    }
    case ValueType_UNDEF:
    {
        printf("undefinded");
        break;
    }
    case ValueType_ARR: //@todo
    {
        double val = *(double *)self->value;
        printf("%lf |", val);
        break;
    }
    }
    ValueType_print(self->type);
}
void Var_print(Var *self);

typedef struct __Program
{
    char *error;
} Program;

Value *Value_newNumber(double number)
{
    double *numberPtr = malloc(sizeof(double));
    *numberPtr = number;
    return Value_new(ValueType_NUMBER, numberPtr);
}

Value * Value_newString(const char * str)
{
    return Value_new(ValueType_STRING, strdup(str));
}


Value * Value_newBool(bool res)
{
    bool * boolean = malloc(sizeof(bool));
    *boolean = res;
    return Value_new(ValueType_BOOL, boolean);
}

double Value_number(Value *self)
{
    assert(self->type == ValueType_NUMBER);
    return *((double *)self->value);
}

bool Value_bool(Value *self)
{
    assert(self->type == ValueType_BOOL);
    return *((bool *)self->value);
}

char * Value_string(Value *self)
{
    assert(self->type == ValueType_STRING);
    return ((char *)self->value);
}

bool Value_equals(Value * a, Value * b)
{
    if(a->type != b->type){
        return false;
    }
    switch(a->type){
        case ValueType_BOOL:
        return Value_bool(a) == Value_bool(b);
        case ValueType_NUMBER:
        return fabs(Value_number(a)-Value_number(b)) < 1e-6;
        case ValueType_STRING:
        return strcmp(Value_string(a),Value_string(b));
        case ValueType_UNDEF:
        return true;
        default: assert(0 && "Not supportet");
    }
}

bool Value_toBool(Value * self)
{
    switch(self->type){
        case ValueType_BOOL:
        return Value_bool(self);
        case ValueType_NUMBER:
        return fabs(Value_number(self)) > 1e-6;
        case ValueType_STRING:
        return Value_string(self) != NULL;
        case ValueType_UNDEF:
        return false;
        default: assert(0 && "Not supportet");
    }
}

Value *eval(Program *program, Tree *node)
{
    AstNode *astNode = node->value;
    switch (astNode->type)
    {
    case AstNodeType_NUMBER:
    {
        double number = atof(astNode->name);
        return Value_newNumber(number);
    }
    case AstNodeType_STRING:
    {
        return Value_newString(astNode->name);
    }
    case AstNodeType_PLUS:
    {
        Tree *firstChild = List_at(node->children, 0);
        Value *firstVal = eval(program, firstChild);
        if (program->error)
        {
            return NULL;
        }
        int nChild = List_count(node->children);
        if (nChild == 1)
        {
            if (firstVal->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid operation");
                Value_free(firstVal);
                return NULL;
            }
            return firstVal;
        }
        else
        {
            Tree *secondChild = List_at(node->children, 1);
            Value *secondVal = eval(program, secondChild);
            if (program->error)
            {
                return NULL;
            }
            if (firstVal->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid operation");
                Value_free(firstVal);
                Value_free(secondVal);
                return NULL;
            }
            if (secondVal->type != ValueType_NUMBER)
            {
                program->error = strdup("Invalid operation");
                Value_free(firstVal);
                Value_free(secondVal);
                return NULL;
            }
            double res = 
            Value_number(firstVal) + 
            Value_number(secondVal);
            Value_free(firstVal);
            Value_free(secondVal);
            return Value_newNumber(res);
        }
    }
    case AstNodeType_MULT:
    {

        Tree *firstChild = List_at(node->children, 0);
        Value *firstVal = eval(program, firstChild);
        if (program->error)
        {
            return NULL;
        }
        if (firstVal->type != ValueType_NUMBER)
        {
            program->error = strdup("Invalid operation");
            Value_free(firstVal);
            return NULL;
        }
        Tree *secondChild = List_at(node->children, 1);
        Value *secondVal = eval(program, secondChild);
        if (program->error)
        {
            return NULL;
        }
        if (secondVal->type != ValueType_NUMBER)
        {
            program->error = strdup("Invalid operation");
            Value_free(firstVal);
            Value_free(secondVal);
            return NULL;
        }
        double res = Value_number(firstVal) * Value_number(secondVal);
        Value_free(firstVal);
        Value_free(secondVal);
        return Value_newNumber(res);
    }
    case AstNodeType_DIV:
    {
        Tree *firstChild = List_at(node->children, 0);
        Value *firstVal = eval(program, firstChild);
        if (program->error)
        {
            return NULL;
        }
        if (firstVal->type != ValueType_NUMBER)
        {
            program->error = strdup("Invalid operation");
            Value_free(firstVal);
            return NULL;
        }
        Tree *secondChild = List_at(node->children, 1);
        Value *secondVal = eval(program, secondChild);
        if (program->error)
        {
            return NULL;
        }
        if (secondVal->type != ValueType_NUMBER)
        {
            program->error = strdup("Invalid operation");
            Value_free(firstVal);
            Value_free(secondVal);
            return NULL;
        }
        
        if (fabs(Value_number(secondVal)) < 0.01)
        {
            program->error = strdup("Cannot divide by zero");
            Value_free(firstVal);
            Value_free(secondVal);
            return NULL;
        }
        double res = Value_number(firstVal) / Value_number(secondVal);
        Value_free(firstVal);
        Value_free(secondVal);
        return Value_newNumber(res);
    }
    case AstNodeType_LOG_AND: {
        Tree *firstChild = List_at(node->children, 0);
        Value *firstVal = eval(program, firstChild);
        if (program->error)
        {
            return NULL;
        }
        Tree *secondChild = List_at(node->children, 1);
        Value *secondVal = eval(program, secondChild);
        if (program->error)
        {
            return NULL;
        }
        bool res = Value_toBool(firstVal) && Value_toBool(secondVal);
        Value_free(firstVal);
        Value_free(secondVal);
        return Value_newBool(res);
    }
    case AstNodeType_LOG_EQ: {
        Tree *firstChild = List_at(node->children, 0);
        Value *firstVal = eval(program, firstChild);
        if (program->error)
        {
            return NULL;
        }
        Tree *secondChild = List_at(node->children, 1);
        Value *secondVal = eval(program, secondChild);
        if (program->error)
        {
            return NULL;
        }
        bool res = Value_equals(firstVal,secondVal);
        Value_free(firstVal);
        Value_free(secondVal);
        return Value_newBool(res);
    }
    default:
    {
        assert(0 && "Not impl");
        break;
    }
    }
    return NULL;
}

int Interpreter_execute(Tree *astTree)
{
    AstNode *astNode = astTree->value;
    assert(astNode->type == AstNodeType_PROGRAM);
    Tree *firstNode = List_at(astTree->children, 0);
    Program program = {
        .error = NULL
    };
    Value * val = eval(&program,firstNode);
    if(program.error)
    {
        fprintf(stderr, "Runtime error : %s", program.error);
        free(program.error);
        return 1;
    }
    Value_print(val);
    Value_free(val);
    return 0;
}
