#pragma once

#include <list.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

bool isInt(char *buf);
bool isDouble(char *buf);