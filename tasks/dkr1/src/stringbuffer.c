#include <stringbuffer.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>

struct __StringBuffer {
    char * buffer;
    size_t capaicty;
    size_t length;
};

static const size_t INITIAL_CAPASITY = 256;

StringBuffer * StringBuffer_new(void){
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capaicty = INITIAL_CAPASITY;
    self->buffer = malloc(sizeof(char) * self->capaicty);
    self->buffer[0] = '\0';
    self->length = 1;
    return self;
}

void StringBuffer_free(StringBuffer * self){
    free(self->buffer);
    free(self);
}


static void ensureCapaicity(StringBuffer* self, int appendLen){
    if(self->length + appendLen > self->capaicty){
        size_t newCapaicity = self->capaicty * 2;
        char * newBuffer = realloc(self->buffer, sizeof(char) * newCapaicity);
        if(newBuffer == NULL){
            assert(newBuffer);
        }else{
            self->buffer = newBuffer;
            self->capaicty = newCapaicity;
        }
    }
}

void StringBuffer_append(StringBuffer * self, const char * str){
    size_t strln = strlen(str);
    ensureCapaicity(self,strln);
    strcat(self->buffer + (self->length - 1),str);
    self->length += strln;
}

void StringBuffer_appendChar(StringBuffer * self, char ch){
    ensureCapaicity(self,1);
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}

void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...){
    va_list vlist;
    va_start(vlist,fmt);
    size_t bufsz = vsnprintf(NULL,0,fmt, vlist);
    char * buf = malloc(bufsz + 1);
    va_start(vlist,fmt);
    vsnprintf(buf,bufsz + 1,fmt,vlist);
    va_end(vlist);
    StringBuffer_append(self,buf);
    free(buf);
}

void StringBuffer_clear(StringBuffer * self){
    self->buffer[0] ='\0';
    self->length = 1;
}
char * StringBuffer_toNewString(StringBuffer * self){
    return strdup(self->buffer);
}