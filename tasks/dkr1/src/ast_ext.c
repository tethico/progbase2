#include <ast_ext.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>


static char * str_append(const char * str, const char * append);
static void Print(Tree * node,const char * indent, int root, int last);

void AstTree_print(Tree * asTree)
{
    char * indent = strdup("");
    Print(asTree,indent,1,1);
    free((void*)indent);
}


static void Print(Tree * node,const char * indent, int root, int last)
{
    printf("%s",indent);
    char * newIndent = NULL;
    if(last){
        if(!root){
            printf("└╴");
            newIndent = str_append(indent,"  ");
        }else
        {
            newIndent = str_append(indent,"");
        }
    }
    else{
        printf("├╴");
        newIndent = str_append(indent,"| ");
    }
    AstNode * astNode = node->value;
    printf("%s\n",astNode->name);
    List * children = node->children;
    size_t count = List_count(children);
    for (int i = 0; i < count; i++)
    {
        void * child = List_at(children,i);
        Print(child,newIndent,0,i == count - 1);
    }
    free((void*)newIndent);
}

static char * str_append(const char * str, const char * append)
{
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf,"%s%s",str,append);
    return buf;
}