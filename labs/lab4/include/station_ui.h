#pragma once

#include "station.h"
#include "string_check_val.h"
#include "station_file.h"
#include "station_string.h"
#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <list.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <string.h>

void menuStart(void);
int DeleteStationStruct_byIndex(int index, List * stations); 
int ChangeStructField(Station *station);                                              
void PrintAllStationsOpenedBeforeX(List * stations, int time_x);
void PrintStructArr(List * stations);
void menu(List * stations);
Station *inputStruct();