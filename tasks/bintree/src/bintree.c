#include <bintree.h>
#include <stdlib.h>

BinTree * BinTree_new(int value){
    BinTree * node = malloc(sizeof(BinTree));
    node->value = value;
    node->left = NULL;
    node->right = NULL;
    return node;
}

void BinTree_free(BinTree * self){
    free(self);
}