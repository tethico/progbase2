#pragma once

#include <QDialog>
#include "stationparser.h"

namespace Ui {
class addDialog;
}

class addDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addDialog(QWidget *parent = 0);
    ~addDialog();
    bool bAccepted = false;
    MetroStation getStation();
private slots:
    void on_buttonBox_accepted();

private:
    MetroStation station;
    Ui::addDialog *ui;
};
