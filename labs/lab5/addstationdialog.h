#ifndef ADDSTATIONDIALOG_H
#define ADDSTATIONDIALOG_H

#include <QDialog>

namespace Ui {
class addStationDialog;
}

class addStationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addStationDialog(QWidget *parent = 0);
    ~addStationDialog();

private:
    Ui::addStationDialog *ui;
};

#endif // ADDSTATIONDIALOG_H
