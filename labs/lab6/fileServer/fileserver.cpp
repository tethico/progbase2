#include "fileserver.h"
#include <QTcpSocket>
#include <QDir>
#include <stationparser.h>
#include <iostream>
fileServer::fileServer(QObject *parent)  : QObject(parent)
{
    server = new QTcpServer(parent);

    connect(server, SIGNAL(newConnection()),
            this, SLOT(onNewConnected()));

    const int PORT = 3000;
    if(!server->listen(QHostAddress::Any, PORT)) {
        std::cout << "Server could not start" << std::endl;
    } else {
        std::cout << "Server started at port " << PORT << std::endl;
    }
}

void fileServer::onNewConnected()
{
    std::cout << "client connected" << std::endl;
    QTcpSocket * socket = server->nextPendingConnection();
    connect(socket, SIGNAL(readyRead()), SLOT(onRequest()));
    connect(socket, SIGNAL(disconnected()), SLOT(onDisconnected()));
}

void fileServer::onDisconnected()
{
    std::cout << "client disconnected" << std::endl;
}

void fileServer::onRequest()
{
    qDebug() << "got Request";
    QTcpSocket * socket = static_cast<QTcpSocket*>(sender());
    QByteArray request = socket->read(1);
    qDebug() << request.toInt();
    StationParser parser;
    switch (request.toInt()) {
    case 0:
    {
        // give file list
        qDebug() << "give file list";
        QDir dir(this->dir);
        QStringList fileList = dir.entryList();
        QString listS;
        int i = 0;
        while(fileList.begin() + i != fileList.end()){
            listS.append(fileList.at(i) + ",");
            i++;
        }
        QByteArray list = listS.toUtf8();
        socket->write("2" + list);
        socket->flush();
    }
        break;
    case 1:
    {
        qDebug() << "add new file to /data";
        // add new file to /data
        QByteArray fn = socket->readLine();
        QString fileName(fn);
        QFile newFile(dir + fileName + ".json");
        if(newFile.open(QIODevice::WriteOnly))
        {
            //file created
        }
        newFile.close();
        break;
    }
    case 2:
    {
         qDebug() << "add new MS to list";
        // add new MS to list
        QByteArray MSjson;
        for(int i = 0; i < 9;i++){
            MSjson.append(socket->readLine());
        }
        MetroStation st = parser.MSFromJson(QString(MSjson));
        puts(QString(MSjson).toStdString().c_str());
        activeServerList.push_back(st);
        socket->write("1" + parser.listToJson(activeServerList).toUtf8());
        socket->flush();
        break;
    }
    case 3:
    {
         qDebug() << "remove MS from list";
        // remove MS from list
        int index = socket->readLine().toInt();
        activeServerList.erase(activeServerList.begin() + index);
        socket->write("1" + parser.listToJson(activeServerList).toUtf8());
        socket->flush();
        break;
    }
    case 4:
    {
        qDebug() << " re'add MS to list";
        // re'add MS to list
        int index = socket->read(1).toInt();
        QByteArray MSjson = socket->readAll();
        MetroStation st = parser.MSFromJson(QString(MSjson));
        activeServerList.replace(index,st);
        socket->write("1" + parser.listToJson(activeServerList).toUtf8());
        socket->flush();
        break;
    }
    case 5:
    {
         qDebug() << " give list back to client in json";
        // give list back to client in json
         qDebug() << parser.listToJson(activeServerList);
        socket->write("1" + parser.listToJson(activeServerList).toUtf8());
        socket->flush();
        break;
    }
    case 6:
    {
        qDebug() << " save to data/___.json";
        // save to data/___.json
        QByteArray nameB = socket->readLine();
        qDebug() << nameB;
        QString name(nameB);
        QFile file(dir + name);
        file.open(QIODevice::WriteOnly);
        file.write(parser.listToJson(activeServerList).toUtf8());
        file.close();
        break;
    }
    case 7:
    {
         qDebug() << "read from data/___.json";
        // get json formatted list to server
         QByteArray nameB = socket->readLine();
         qDebug() << nameB;
         QString name(nameB);
         QFile file(dir + name);
         file.open(QIODevice::ReadOnly);
         QByteArray fileContent = file.readAll();
         activeServerList = parser.listFromJson(QString(fileContent));
         socket->write("1" + parser.listToJson(activeServerList).toUtf8());
         socket->flush();
        break;
    }
    default:
    {
        std::cout << "Wrong command"<< std::endl;
        break;
    }
    }
}
