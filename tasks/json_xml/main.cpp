#include <iostream>
#include <fstream>
#include <qfile.h>
#include <vector>
#include <qtextstream.h>
#include <entitystorage.h>
#include <storagemanager.h>
#include <scrummaster.h>
#include <QtXml>
#include <messageexception.h>

using namespace std;

void printJsonFileFromVector(std::vector<ScrumMaster> &entities);
void printXmlFileFromVector(std::vector<ScrumMaster> &entities);
void printFakeFileFromVector(std::vector<ScrumMaster> &entities);

int main ()
{
    vector<ScrumMaster> vec;
    vector<string> names1;
    names1.push_back("Helena");
    names1.push_back("Vlad");
    names1.push_back("Polyak");
    names1.push_back("Roman");
    names1.push_back("Lisa");
    ScrumMaster * master1 = new ScrumMaster("Zoe",21,4.8,names1);
    vec.push_back(*master1);
    vector<string> names2;
    names2.push_back("Lele");
    names2.push_back("Igor");
    names2.push_back("Joe");
    names2.push_back("Will");
    ScrumMaster * master2 = new ScrumMaster("Kyle",32,4.1,names2);
    vec.push_back(*master2);
    vector<string> names3;
    names3.push_back("Lotric");
    names3.push_back("Andy");
    names3.push_back("Terry");
    ScrumMaster * master3 = new ScrumMaster("Moe",26,3.4,names3);
    vec.push_back(*master3);
    vector<EntityStorage *> storgManagers;

    EntityStorage * xml = NULL;
    try{
        xml = StorageManager::createStorage("data.xml");
    }catch(MessageException &err){
        cerr << err.what() << endl;
    }
    storgManagers.push_back(xml);

    EntityStorage * json = NULL;
    try{
        json = StorageManager::createStorage("data.json");
    }catch(MessageException &err){
        cerr << err.what() << endl;
    }
    storgManagers.push_back(json);

    EntityStorage * fake = NULL;
    try{
        fake = StorageManager::createStorage("data.fake");
    }catch(MessageException &err){
        cerr << err.what() << endl;
    }
    storgManagers.push_back(fake);
    for(unsigned int i = 0; i < storgManagers.size(); i++){
        try{
            if(storgManagers.at(i) != NULL){
                storgManagers.at(i)->save(vec);
            }else{
                string error = "No storage to [save] to";
                throw(MessageException(error));
            }
        }catch(MessageException &err){
            cerr << err.what() << endl;
        }
    }
    getchar();
    string errorLoad = "Cannot load vector of entities from this storage manager";
    MessageException errorLoadExep = MessageException(errorLoad);
    vector<ScrumMaster> emptyVec;
    try{
        vec = storgManagers.at(0) == NULL ? throw(errorLoadExep) : storgManagers.at(0)->load();
        printXmlFileFromVector(vec);
    }catch(MessageException &err){
        err.what();
    }
    try{
        vec = storgManagers.at(1) == NULL ? throw(errorLoadExep) : storgManagers.at(1)->load();
        printJsonFileFromVector(vec);
    }catch(MessageException &err){
        err.what();
    }
    try{
        vec = storgManagers.at(2) == NULL ? throw(errorLoadExep) : storgManagers.at(2)->load();
        printFakeFileFromVector(vec);
    }catch(MessageException &err){
        err.what();
    }
    free(xml);
    free(json);
    free(fake);
    delete(master1);
    delete(master2);
    delete(master3);
    return 0;
}

void printJsonFileFromVector(std::vector<ScrumMaster> &entities)
{
    if(entities.size() == 0){
        string errorPrint = "Cannot load vector of entities from this storage manager";
        MessageException errorPrintExep = MessageException(errorPrint);
        throw(errorPrintExep);
        return;
    }
    cout << "data.json" << endl;
    QJsonDocument doc;
    QJsonObject array;
    QJsonArray scrumMasters;
    for(unsigned int i = 0; i < entities.size(); i++)
    {
        QJsonObject master;
        master.insert("name",entities.at(i).name.c_str());
        master.insert("age",entities.at(i).age);
        master.insert("efficency mark",entities.at(i).efficencyMark);
        //aray
        QJsonArray team;
        for(unsigned int j = 0; j < entities.at(i).team.size(); j++){
            team.append(entities.at(i).team[j].c_str());
        }
        master.insert("team",team);
        scrumMasters.append(master);
    }
    array.insert("Scrum Masters", scrumMasters);
    doc.setObject(array);
    std::cout << doc.toJson().toStdString().c_str() << endl;
}

void printXmlFileFromVector(std::vector<ScrumMaster> &entities)
{
    if(entities.size() == 0){
        string errorPrint = "Cannot load vector of entities from this storage manager";
        MessageException errorPrintExep = MessageException(errorPrint);
        throw(errorPrintExep);
        return;
    }
    cout << "data.xml" << endl;
    QDomDocument doc;
    QDomElement array = doc.createElement("ScrumMasters");
    for(unsigned int i = 0; i < entities.size(); i++)
    {
        QDomElement master = doc.createElement("ScrumMaster");
        master.setAttribute("age",entities.at(i).age);
        master.setAttribute("name",entities.at(i).name.c_str());
        master.setAttribute("efficencyMark",entities.at(i).efficencyMark);
        QDomElement team = doc.createElement("team");
        for(unsigned int j = 0;j < entities.at(i).team.size();j++)
        {
            QDomElement member = doc.createElement("member");
            member.setAttribute("name",entities.at(i).team[j].c_str());
            team.appendChild(member);
        }
        master.appendChild(team);
        array.appendChild(master);
    }
    doc.appendChild(array);
    std::cout << doc.toString().toStdString().c_str() << std::endl;
}

void printFakeFileFromVector(std::vector<ScrumMaster> &entities){
    if(entities.size() == 0){
        string errorPrint = "Cannot load vector of entities from this storage manager";
        MessageException errorPrintExep = MessageException(errorPrint);
        throw(errorPrintExep);
        return;
    }
    cout << "data.fake" << endl;
}
/*
 * ../json_xml/..
 *
 *
ofstream myfile;
 myfile.open ("example.txt");
 myfile << "Writing this to a file.\n";
 myfile.close();
*/
