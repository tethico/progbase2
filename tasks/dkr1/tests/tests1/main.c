#include <list.h>
#include <lexer.h>
#include <check.h>

START_TEST (split_empty_empty)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 0);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "13" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneDoubleNumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13.3", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "13.3" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_TwoDots_NumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13.3.3", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 2);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token * secondToken = (Token *)List_at(tokens, 1);
   Token testToken = { TokenType_NUMBER, "13.3" };
   Token testToken2 = { TokenType_NUMBER, "3" };
   ck_assert(Token_equals(firstToken, &testToken));
   ck_assert(Token_equals(secondToken, &testToken2));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_2Tokens)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("0Pele", tokens);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token * secondToken = (Token *)List_at(tokens, 1);
   Token testToken1 = { TokenType_NUMBER, "0" };
   Token testToken2 = { TokenType_NAME, "Pele" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2));
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 2);
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST



START_TEST (split_onePow_onePowToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("^", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_POW, "pow" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

Suite *test_suite(void) {
  Suite *s = suite_create("Module");
  TCase *tc_sample;

  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tc_sample = tcase_create("TestCase");
  tcase_add_test(tc_sample, split_2Tokens);
  tcase_add_test(tc_sample, split_empty_empty);
  tcase_add_test(tc_sample, split_oneDoubleNumberToken);
  tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
  tcase_add_test(tc_sample, split_onePow_onePowToken);
  tcase_add_test(tc_sample, split_TwoDots_NumberToken);

  suite_add_tcase(s, tc_sample);

  return s;
}

int main(void) {
  Suite *s = test_suite();
  SRunner *sr = srunner_create(s);
  srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

  srunner_run_all(sr, CK_VERBOSE);

  int num_tests_failed = srunner_ntests_failed(sr);
  srunner_free(sr);
  return num_tests_failed;
}

